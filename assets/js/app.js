/*
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee *
 */


 /**
     * You must include the dependency on 'ngMaterial' 
     */


   AVRapp = angular.module('AVRapp', ['ngMaterial','chart.js','ngMap']);

    AVRapp.config(function($mdIconProvider) {
      $mdIconProvider.defaultIconSet(assets_path+'css/dependencies/fonts/mdi.svg')
    })

    .controller('passCtrl',function($scope,$http,$httpParamSerializerJQLike,$rootScope){
        $scope.loading = false;
        $scope.logout = $rootScope.logout;
        $scope.ok = $rootScope.ok;
        $scope.info = $rootScope.info;
        $scope.fail = $rootScope.fail;

        $scope.updatePass = function(){
            if($scope.passForm.$invalid)
                return;

            $scope.loading = true;

            var data = {old_pass:$scope.old_pass,new_pass:$scope.new_pass};


            $http({
                method:'post',
                url: site_path+"login/change_password",
                data: $httpParamSerializerJQLike(data),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
            })
                .success(function(data){
                    $scope.loading = false;
                    $rootScope.passwordChange.close();
                    if(data == 'ok'){
                        $scope.ok('Password successfully changed');
                        $scope.logout();
                    }
                    else $scope.fail();
                })
                .error(function(){
                    $scope.loading = false;
                    $rootScope.passwordChange.close();
                    $scope.fail('error connecting')
                });

        }

    });

AVRapp.run(function($rootScope,$mdToast,$window,$http,$mdComponentRegistry,$mdSidenav,$httpParamSerializerJQLike,$mdDialog){


        $rootScope.passwordChange = {
            template: '<md-dialog aria-label="Password Change" layout="column" >' +
            '<md-toolbar><div class="md-toolbar-tools"><h2>Change Password</h2></div></md-toolbar>' +
            '<form ng-submit="updatePass()" name="passForm">' +
            '<md-dialog-content>' +
            '<md-progress-linear ng-show="loading" md-mode="query"></md-progress-linear>' +
            '<md-card layout="column" layout-padding flex >' +
            '<md-input-container><input placeholder="Old Password" type="password" ng-model="old_pass" ng-required="true"></md-input-container>' +
            '<md-input-container><input placeholder="New Password" type="password" ng-model="new_pass" ng-required="true"></md-input-container>' +
            '<md-input-container><input placeholder="Repeat Password" type="password" ng-model="rep_pass" ng-required="true" compare-to="new_pass"></md-input-container>' +
            '</md-card></md-dialog-content>' +
            '<md-dialog-actions>' +
            '<md-button type="submit" class="md-primary" ">Submit</md-button>' +
            '</md-dialog-actions>' +
            '</form>'+
            '</md-dialog>',

            show: function(){

                $mdDialog.show({
                    controller: 'passCtrl',
                    template: $rootScope.passwordChange.template,
                    clickOutsideToClose:true,
                    escapeToClose:true,
                    fullscreen:false,
                    preserveScope:true,
                });
            },
            close: function(){
                $mdDialog.cancel();
            }
        }

    $rootScope.changePass = $rootScope.passwordChange.show;


        $rootScope.repMap = {
                template: '' +
                '<md-dialog aria-label="map" layout="column" layout-padding flex="80" style="height:500px;">' +
                '<md-card layout="column" ><ng-map ng-if="mapLoad" center="{{report.gps}}" zoom="14" flex="100">' +
                '<marker position="{{report.gps}}" ></marker>' +
                '</ng-map></md-card>' +
                '<div class="md-caption">{{report.gps}}</div>' +
                '<md-dialog-actions layout="row" ng-hide="hideMapButtons" layout-align="space-between center" ng-hide="previewMap" >' +
                '<div class="md-button md-primary " ng-click="approveMap(this)">Approve</div>' +
                '<div class="md-button md-accent " ng-click="closeMap()">Reject</div>' +
                '</md-dialog-actions>' +
                '</md-dialog>',
                show: function(scope,preview){
                    preview = preview||false;
                    var clickoutside = preview?true:false;
                    scope.previewMap = preview;

                    $mdDialog.show({
                        template: this.template,
                        clickOutsideToClose:clickoutside,
                        escapeToClose:clickoutside,
                        fullscreen:false,
                        scope:scope,
                        preserveScope:true,
                    });
                },
                close: function(){
                    $mdDialog.cancel();
                }
        }

            $rootScope.do_export = function(type,request_id,scope){
                scope.loading = true;

                //todo RESTRUCTURE FOR PDF EXPORTS
                console.log(type)
                if(type == 'pdf'){
                    if(typeof(request_id)=='array'||typeof(request_id)=='object')
                        request_id = request_id[0]||0;
                    //Delay the loading removal
                    setTimeout(
                        function(){
                            scope.loading = false;
                        },500);
                    window.location.href = ctrl_path+'/print_pdf_report/'+request_id;
                    return;
                }



                request_id = typeof(request_id)!='array'&&typeof(request_id)!='object'?[request_id]:request_id
                var data = {requests:request_id};
                $http({
                    url:ctrl_path+'/export_request/',
                    method:'post',
                    data:$httpParamSerializerJQLike(data),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                })
                    .success(
                        function(data){
                            if(typeof(data) != 'object' || data.length<1)
                                return $rootScope.empty('Unable to fetch Data, Please try again');

                            $rootScope[type+'_export'](data);
                        })
                    .error(function(e){
                            console.log(e)
                        })
                    .then(
                        function(){
                            scope.loading = false;
                    })

            }

            $rootScope.excel_export = function(data){
                var req_keys = Object.keys(data[0]);
                req_keys.splice(req_keys.indexOf('report'),1);
                var rep_keys = Object.keys(data[0].report);
                rep_keys.splice(rep_keys.indexOf('interviewee'),1);
                var int_keys = Object.keys(data[0].report.interviewee);

                var fin = [],
                    hdr = req_keys;

                //hdr.concat(rep_keys);
                rep_keys.map(function(k){
                    hdr.push(k);
                })

                int_keys.map(function(k){
                    hdr.push('interviewee_'+k);
                })

                fin[0] = hdr;

                var to_exp = (function(){
                    data.map(function(a,b,c){
                        var rep = a.report;
                        delete(a.report)
                        Object.assign(a,rep);
                        var int = a.interviewee;
                        for(i in int){
                            int['interviewee_'+i] = int[i];
                            delete(int[i])
                        }
                        Object.assign(a,int);
                        delete(a.interviewee)

                    })

                    data.forEach(function(a){
                        var k = Object.keys(a);
                        var arval = [];

                        var kv=0;
                        k.forEach(function(v){
                            arval[kv++] = a[v]
                        })
                        fin.push(arval);
                    })

                    return data
                })();
                export_data_to_excel(fin)
            }


            $rootScope.pdf_export = function(data){

                var to_exp = (function() {
                    data.map(function (a, b, c) {
                        var rep = a.report;
                        delete(a.report)
                        Object.assign(a, rep);
                        var int = a.interviewee;
                        for (i in int) {
                            int['interviewee_' + i] = int[i];
                            delete(int[i])
                        }
                        Object.assign(a, int);
                        delete(a.interviewee)

                    })
                })();



                var options={
                    'page-break':'page-break-after: always;'
                }

                var pre_print = '<div style="width:595px;margin:0 auto;font-size:24px;background-color:#fff;">';
                //var pre_print = '<body style="width:800px;margin:0 auto;font-size:24px;">';
                var post_print = '</div>';

                var to_print = '\
                    <div style="__page_break width:595px;" id="">\
                    <!--Header-->\
                    <div style="height:50px;width:100%;margin-top:15px;">\
                    <div style="width:25%;height:100%;float:left;"><img style="max-height:100%;max-width:100%;margin:0 auto;display:block;" src="'+assets_path+'img/logo.png"></div>\
                    <div style="width:50%;height:100%;float:left;"><p align="center" style="font-size:0.7em"><span style="font-weight:bold">'+licensed+'</span><br>'+address+'</p></div>\
                    <div style="width:25%;height:100%;float:left;"><img style="max-height:100%;max-width:100%;margin:0 auto;display:block;" src="'+assets_path+'uploads/logo/'+logo+'"></div>\
                    </div>\
                    <!--Body-->\
                    \
                    <div style="width:595px;font-size:0.9em;line-height: 24px;margin-top:20px;border:1px solid;border-color: #ddd !important;display:table;border-collapse: collapse;">\
                    \
                    <div style="display:table-header-group;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;font-size:1em;font-weight:bold;font-family:Verdana" align="center">ADDRESS VERIFICATON REPORT FORM</div>\
                    <div style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;font-size:0.9em;font-weight:bold;font-family:Segoe UI;padding-left:7px;" align="left">Request Details</div>\
                    \
                    <div style="display:table-row;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:30%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Branch ID:</span><span>__branch_id</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:30%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Customer\'s ID:</span><span>__customer_id</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:40%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Date:</span><span>__request_date</span></div>\
                    </div>\
                    </div>\
                    \
                    <div style="display:table-row;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:60%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Full Name:</span><span>__name</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:40%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Phone Number(s):</span><span>__telephone</span></div>\
                    </div>\
                    </div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;min-height: 40px;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-left-width: 0 !important;width:50%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Address:</span><span>__address</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-right-width: 0 !important;width:50%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Nearest B/Stop or Landmark:</span><span>__landmark</span></div>\
                    </div>\
                    </div>\
                    \
                    \
                    <div style="display:block;height:7px"></div>\
                    \
                    <div style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;font-size:0.9em;font-weight:bold;font-family:Segoe UI;padding-left:7px;" align="left">Report Details</div>\
                    \
                    <div style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;font-size:0.7em;font-family:Verdana;padding-left:7px;" align="left"><span style="font-weight:bold;font-family:calibri">Summary:</span><span>__summary</span></div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:40%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Address Exists:</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Yes:</span>__address_exists:yes</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">No:</span>__address_exists:no</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Inconclusive:</span>__address_exists:inconclusive</div>\
                    </div>\
                    </div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:25%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Customer Resides:</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:18.75%;padding-left:7px;"><span style="font-family:calibri">Yes:</span>__resides:yes</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:18.75%;padding-left:7px;"><span style="font-family:calibri">No:</span>__resides:no</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:18.75%;padding-left:7px;"><span style="font-family:calibri">Moved:</span>__resides:moved</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:18.75%;padding-left:7px;"><span style="font-family:calibri">Locked:</span>__resides:locked</div>\
                    </div>\
                    </div>\
                    \
                    <div style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;font-size:0.7em;font-family:Verdana;padding-left:7px;" align="left"><span style="font-weight:bold;font-family:calibri">Description:</span><span>__description</span></div>\
                    \
                    \
                    <div style="display:block;height:7px"></div>\
                    \
                    <div style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;font-size:0.9em;font-weight:bold;font-family:Segoe UI;padding-left:7px;" align="left">Building Details</div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:15px;padding-left:7px;"></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:24%;padding-left:7px;"><span style="font-family:calibri">Bare Land:</span>__completion:bare_land</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:19%;padding-left:7px;"><span style="font-family:calibri">Temporary:</span>__completion:temporary</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:19%;padding-left:7px;"><span style="font-family:calibri">Uncompleted:</span>__completion:uncompleted</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:19%;padding-left:7px;"><span style="font-family:calibri">Plastered:</span>__completion:plastered</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:19%;padding-left:7px;"><span style="font-family:calibri">Completed:</span>__completion:completed</div>\
                    </div>\
                    </div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:15px;padding-left:7px;"></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">Bungalow:</span>__structure_0:bungalow</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">One Storey:</span>__structure_0:one_storey</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">Two Storeys:</span>__structure_0:two_storey</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">Low Rise:</span>__structure_0:low_rise</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">Mid Rise:</span>__structure_0:mid_rise</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">High Rise:</span>__structure_0:high_rise</div>\
                    </div>\
                    </div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:15px;padding-left:7px;"></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">Attached:</span>__structure_1:attached</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">Semi Detached:</span>__structure_1:semi_detached</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">Detached:</span>__structure_1:detached</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">Terraced:</span>__structure_1:terraced</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">Movable:</span>__structure_1:movable</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Others:</span>__structure_1:others</div>\
                    </div>\
                    </div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:15px;padding-left:7px;"></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:49.8%;padding-left:7px;"><span style="font-family:calibri">Single Unit:</span>__structure_2:single_unit</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:49.8%;padding-left:7px;"><span style="font-family:calibri">Multiple Units:</span>__structure_2:multiple_unit</div>\
                    </div>\
                    </div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:15%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Finishing:</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:11.6%;padding-left:7px;"><span style="font-family:calibri">Ceramic:</span>__finish:ceramic</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:11.6%;padding-left:7px;"><span style="font-family:calibri">Glass:</span>__finish:glass</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:11.6%;padding-left:7px;"><span style="font-family:calibri">Paint:</span>__finish:paint</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:11.6%;padding-left:7px;"><span style="font-family:calibri">Plaster:</span>__finish:plaster</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:11.6%;padding-left:7px;"><span style="font-family:calibri">Wallpaper:</span>__finish:wallpaper</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:11.6%;padding-left:7px;"><span style="font-family:calibri">Wood:</span>__finish:wood</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:15%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Color:</span><span>__color</span></div>\
                    </div>\
                    </div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:25%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Area Profile:</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:25%;padding-left:7px;"><span style="font-family:calibri">Low:</span>__area:low</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:25%;padding-left:7px;"><span style="font-family:calibri">Medium:</span>__area:medium</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:25%;padding-left:7px;"><span style="font-family:calibri">High:</span>__area:high</div>\
                    </div>\
                    </div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;;border-left-width: 0 !important;width:20%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Ownership:</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Landlord:</span>__owner:landlord</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Relation:</span>__owner:relation</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Tenant:</span>__owner:tenant</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-right-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Other:</span>__owner:other</div>\
                    </div>\
                    </div>\
                    \
                    \
                    <div style="display:block;height:7px"></div>\
                    \
                    <div style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;font-size:0.9em;font-weight:bold;font-family:Segoe UI;padding-left:7px;" align="left">Interviewee Details</div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:20%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Individual Interviewed:</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Customer:</span>__interviewed:customer</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Others:</span>__interviewed:others</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">None:</span>__interviewed:none</div>\
                    </div>\
                    </div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:20%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">If Other,</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:40%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Full Name:</span><span>__interviewee_name</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:40%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Relationship:</span><span>__interviewee_relationship</span></div>\
                    </div>\
                    </div>\
                    \
                    <div style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;font-size:0.7em;font-family:Verdana;padding-left:7px;" align="left"><span style="font-weight:bold;font-family:calibri">Address:</span><span>__interviewee_address</span></div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-left-width: 0 !important;width:20%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Telephone:</span><span>__interviewee_phone</span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-right-width: 0 !important;width:40%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Gender:</span><span>__interviewee_gender</span></div>\
                    </div>\
                    </div>\
                    \
                    \
                    <div style="display:block;height:7px"></div>\
                    \
                    <div style="border:1px solid;border-color: #ddd !important;border-left-width: 0 !important;border-right-width: 0 !important;font-size:0.7em;font-family:Verdana;padding-left:7px;" align="left"><span style="font-weight:bold;font-family:calibri">GPS CO-ORDINATES:</span><span>__gps</span></div>\
                    \
                    \
                    <div style="display:block;height:7px"></div>\
                    \
                    <div style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;border-right-width: 0 !important;font-size:0.7em;font-family:Verdana;padding-left:7px;" align="left"><span style="font-weight:bold;font-family:calibri">Cerification:</span>\
                    I hereby certify that I have visited the above address of the above named customer and the information presented in this form is true and accurate to the best of my abilities</div>\
                    \
                    <div style="display:table-row;border-color: #ddd !important;" >\
                    <div style="width:100%;line-height: 1.4em;display:table;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-left-width: 0 !important;width:10%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Verifier: </span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:20%;padding-left:7px;">'+licensed.toUpperCase()+' TEAM</div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;width:30%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Sign:</span><span><img style="max-height:30px;max-width:100%;margin:0 auto;display:inline-block;" src="'+assets_path+'img/sign.png"></span></div>\
                    <div style="display:table-cell;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-right-width: 0 !important;width:30%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Date:</span><span>__date_approved</span></div>\
                    </div>\
                    </div>\
                    \
                    \
                    \
                    </div>\
                ';


                var body = '';

                var pb = to_print.indexOf('__page_break');
                if(data.length>1)
                    to_print = to_print.replace('__page_break',options['page-break']);
                else
                    to_print = to_print.replace('__page_break','');

                for(tp in data){
                    body+= fillUp(data[tp],to_print);
                }

                //document.getElementById('print_editor').innerHTML = body;

                var doc = new jsPDF('p','pt','letter');

                // #TODO Better PDF export

               // var width = doc.internal.pageSize.width;
                //var height = doc.internal.pageSize.height;

                //doc.internal.scaleFactor = 2.25;
                //doc.internal.scaleFactor = 2.25;

                //doc.canvas.height = 72 * 11;
                //doc.canvas.width = 72 * 8.5;

               // doc.canvas.height = 72 * 11;
                // doc.canvas.width = 72 * 8.5;
                html2pdf(body, doc, function(doc){
                  //  console.log(doc);
                   // var iframe = document.createElement('iframe');
                   // iframe.setAttribute('style','position:absolute;right:0; top:0; bottom:0; overflow:scroll; height:100%; width:800px');
                   // document.body.appendChild(iframe);
                   // iframe.src = doc.output('datauristring');
                    doc.save('Report.pdf');
                });


            /*    var pdf = new jsPDF("p", "mm", "a4");
                margins = {
                    top: 10,
                    bottom: 10,
                    left: 10,
                    width: 600
                };
                pdf.setProperties({
                    title: title ? title : plenty_admin.context,
                    subject: subject ? subject : plenty_admin.context,
                    author: 'admin.plenty.rdthree.com',
                    creator: '© Round Three LLC 2015'
                });
                $(printWindow.document.body).css("width", "1500px");
                pdf.addHTML(printWindow.document.body,10,10,{pagesplit:true},function() {
                    pdf.save("report_"+moment().format("MM-DD-YYYY h:mma")+'.pdf');
                }, margins);


                doc.addHTML(document.getElementById('print_editor'),10,10,{pagesplit:true},function() {
                    doc.save('Report.pdf');
                    doc.autoPrint();
                });
/*

                 We'll make our own renderer to skip this editor
                var specialElementHandlers = {
                    '#print_editor': function(element, renderer){
                        console.log(element,renderer)
                        return true;
                    }
                };

                // All units are in the set measurement for the document
                // This can be changed to "pt" (points), "mm" (Default), "cm", "in"
                doc.fromHTML(angular.element(document.getElementById('print_editor'))[0], 15, 15, {
                    'width': 1100,
                    'elementHandlers': specialElementHandlers
                });
                 */

                //doc.autoPrint();




                function fillUp(record,str){

                    var rkeys = Object.keys(record);
                    for(k in rkeys){
                        var k = rkeys[k];
                        var matches = str.match(new RegExp('__'+k+'[a-z_:]*','gi'));

                        for(m in matches) {
                            var strt = str.indexOf(matches[m]),
                                stp = strt+matches[m].length;
                            var e = matches[m].length;
                            if(matches[m].indexOf(':')>-1){
                               e = matches[m].indexOf(':');
                            }
                            if(matches[m].substring(2,e) != k)
                                continue;

                            if(matches[m].indexOf(':') == -1){
                                var val = (record[k] == null || record[k] == 'null')?'':record[k];
                                if(matches[m].indexOf('date') > -1 && val != '')
                                    val = new Date(val).toLocaleString();
                                str = str.replace(matches[m],' '+val);
                            }
                            else{
                                var val = (record[k] == null || record[k] == 'null')?'':record[k];
                                val = val.replace(' ','_');
                                val = (matches[m].toLowerCase() == ('__'+k+':'+val).toLowerCase())?'X':'';
                                str = str.replace(matches[m],' '+val)
                            }

                        }
                    }
                    return str;
                }


            }


		    $rootScope.ok = function(txt){
                txt = (typeof txt !== undefined && txt > '')?txt : 'Good! Successful';
                $mdToast.show($mdToast.simple().content(txt).theme('success'));
                  }
            $rootScope.empty = function(txt){
                txt = (typeof txt !== undefined && txt > '')?txt : 'Field(s) empty';
                $mdToast.show($mdToast.simple().content(txt).theme('warn'));
                  }
            $rootScope.notexist = function(txt){
                txt = (typeof txt !== undefined && txt > '')?txt :'Ooh! Doesn\'t exist. Please check for errors and try again';
                $mdToast.show($mdToast.simple().content(txt).theme('fail'));
                  }
            $rootScope.fail = function(txt){
                txt = (typeof txt !== undefined && txt > '')?txt : 'Oops! failed. Please try again';
                $mdToast.show($mdToast.simple().content(txt).theme('fail'));
                  }
            $rootScope.info = function(txt){
                txt = (typeof txt !== undefined && txt > '')?txt : 'Hmm!!!';
                $mdToast.show($mdToast.simple().content(txt).theme('info'));
                  }
                  
                  if(typeof(menu) === 'undefined'){
                  menu = '';
                    }
                  $rootScope.menus = menu;
                  
                  $rootScope.ismenuselected = function(link){
                      if($window.location.href.indexOf(link) > -1)return true;
                      else return false;
                  }
                  
                  $rootScope.logout = function(loc){
                      $http.get(site_path+"login/logout")
                              .success(function(data){
                                  if(data == 'ok'){
                                      $rootScope.info('You have successfully logged out');
                                      var loc = window['loc'];
                              if(typeof(loc) != 'undefined')$window.location.href = loc;
                                      else setTimeout(function(){$window.location.reload()},700);
                                  }
                                  else $rootScope.fail();
                      })
                              .error(function(){$rootScope.fail('error connecting')});
                  }
                  
                  $rootScope.ordertype = 
                  [{term:'name',name:'Request Name'},
                  {term:'address',name:'Request Address'},
                  {term:'request_created',name:'Request Date'},
                  {term:'date_created',name:'Upload Date'},
                  {term: 'dispatch',name:'Assigned Dispatch'}
                  ];
                  
                  $rootScope.searchtype =
                  [{term:'name',name:'Request Name'},
                  {term:'address',name:'Request Address'}
                  ];
                  
                  $rootScope.searchmodel = function(){
                  return 'src.'+$rootScope.sT
                  }
                  
                  $rootScope.toggle_order = function(){
                  	$rootScope.oR =  ($rootScope.oR)?false:true;
                  };
                  
                  $rootScope.oT = 'date_created';
                  $rootScope.sT = 'name';
                  $rootScope.oR = true;
                  $rootScope.src = {};

            $rootScope.page_change = function(link){
            $rootScope.loading = true;
            $rootScope.info('Loading');
            setTimeout(function(){$window.location.href = ctrl_path+link;},500);
            }




    function debounce(func, wait, context) {
        var timer;
        return function debounced() {
            var context = $scope,
                args = Array.prototype.slice.call(arguments);
            $timeout.cancel(timer);
            timer = $timeout(function() {
                timer = undefined;
                func.apply(context, args);
            }, wait || 10);
        };
    }

    function buildToggler(navID) {
        return function() {
            // Component lookup should always be available since we are not using `ng-if`
            $mdSidenav(navID)
                .toggle()
                .then()
        }
    }

    $rootScope.isOpenLeft = function(){
        return $mdSidenav('left').isOpen();
    };

    $rootScope.toggleLeft = buildToggler('left');

})

    .config(function($mdThemingProvider) {
        $mdThemingProvider
            .theme('default')
            .primaryPalette('indigo', {
                'default': '500', // by default use shade 400 from the pink palette for primary intentions
                'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
                'hue-2': '400', // use shade 600 for the <code>md-hue-2</code> class
                'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
            })
            .accentPalette('lime')
            .warnPalette('pink')
            .backgroundPalette('grey', {
                'default': 'A100'
            })
    })

    /*.config(function($mdThemingProvider) {
     $mdThemingProvider
     .theme('default')
     .primaryPalette('blue', {
     'default': '700', // by default use shade 400 from the pink palette for primary intentions
     'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
     'hue-2': '400', // use shade 600 for the <code>md-hue-2</code> class
     'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
     })
     .accentPalette('amber')
     .warnPalette('orange')
     .backgroundPalette('grey');
     })*/

    //Configure charts
    .directive('chartType',function($compile){
        return {
            link: function(scope, element) {
                var template = element[0].outerHTML;
                template=template.replace('__chart-type',scope.chart.chartclass);
                template=template.replace('chart-type=""','');
                var parent = element.parent();
                element.remove();
                parent.append($compile(template)(scope));
            }
        };
    })

    .directive('compareTo',function(){
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function(scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function(modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function() {
                    ngModel.$validate();
                });
            }
        };
    });
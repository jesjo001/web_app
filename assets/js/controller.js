/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */



AVRapp.controller('loginCtrl',['$scope','$http','$window','$animate','$httpParamSerializerJQLike',
    function($scope,$http,$window,$animate,$httpParamSerializerJQLike){
        $scope.loginFormdata = new FormData();
        $scope.loginFormdata.uname;
        $scope.loginFormdata.pass;
        $scope. loginFormdata.remember=false;
        $scope.loading = false;
        $scope.notSubmit = false;
        
        
        
                $scope.doLogin = function(){
                
                $scope.loading = true;
               // $scope.notSubmit = true;
                $http({
                method:'post',
                url: ctrl_path+'logon',
                data: $httpParamSerializerJQLike($scope.loginFormdata),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                })
                .success(function(data){
                $scope.loading = false;
                if(data == 'ok')setTimeout($window.location.reload(),1200);
                if(data == 'notexist'){
                    $scope[data]('Sorry that account does not exist');
                    $scope.logon.uname.$setValidity("required", false);
                    $scope.loginFormdata.pass = '';
                }
                else{ $scope[data]();
                $scope.loginFormdata.pass = '';}
                })
                .error(function(){
                 $scope.loading = false;
                 $scope.notSubmit = false;
                 $scope.fail('error connecting');
                 });  
                
                }
                
                
}]);
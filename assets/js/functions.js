/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 * License subject to changes based on agreement between  Author and Licensee *
 */


function page_change(loc){
    if(loc != '')
        window.location.href=loc;
}

function Workbook() {
    if(!(this instanceof Workbook)) return new Workbook();
    this.SheetNames = [];
    this.Sheets = {};
}

function s2ab(s) {
    var buf = new ArrayBuffer(s.length);
    var view = new Uint8Array(buf);
    for (var i=0; i!=s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
    return buf;
}

function sheet_from_array_of_arrays(data, opts) {
    var ws = {};
    var n = data.length;
    var l = Object.keys(data[0]).length;
    var f = n>l?n:l;

    var range = {s: {c:f, r:f}, e: {c:0, r:0 }};

    for(var R = 0; R < data.length; R++) {
        for(var C = 0; C < data[R].length; C++) {

            if(range.s.r > R) range.s.r = R;
            if(range.s.c > C) range.s.c = C;
            if(range.e.r < R) range.e.r = R;
            if(range.e.c < C) range.e.c = C;

            var cell = {v: data[R][C] };
            if(cell.v == null) continue;

            var cell_ref = XLSX.utils.encode_cell({c:C,r:R});

            if(typeof cell.v === 'number') cell.t = 'n';
            else if(typeof cell.v === 'boolean') cell.t = 'b';
            else if(cell.v instanceof Date) {
                cell.t = 'n'; cell.z = XLSX.SSF._table[14];
                cell.v = datenum(cell.v);
            }
            else cell.t = 's';

            ws[cell_ref] = cell;
        }
    }
    if(range.s.c < 10000000) ws['!ref'] = XLSX.utils.encode_range(range);

    return ws;
}

function export_data_to_excel(data){
    //var oo = data;
    //var ranges = oo[1];

    /* original data */
    //var data = oo[0];
    //var ws_name = "SheetJS";
    //var ws_name = document.title;
    var ws_name = "Sheet";
    //console.log(data);

    var wb = new Workbook(),
    ws = sheet_from_array_of_arrays(data);

    /* add ranges to worksheet */
//    ws['!merges'] = ranges;

    /* add worksheet to workbook */
    wb.SheetNames.push(ws_name);
    wb.Sheets[ws_name] = ws;

    var wbout = XLSX.write(wb, {bookType:'xlsx', bookSST:false, type: 'binary'});

    saveAs(new Blob([s2ab(wbout)],{type:"application/octet-stream"}), Date.now()+".xlsx")
}
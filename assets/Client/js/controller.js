/*
 * Copyright (c)
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony
 * Email: Adibas03@gmail.com
 * Date: 10/2016
 * Licensed to: LicensedTo
 * License subject to changes based on agreement between  Author and Licensee
 */



AVRapp.controller('ClientCtrl',['$scope','$http','$window','$animate','$httpParamSerializerJQLike','$timeout','$mdMedia',
    function($scope,$http,$window,$animate,$httpParamSerializerJQLike,$timeout,$mdMedia){
        
        
        
        
        $scope.loading = true;
        $scope.notSubmit = false;
        $scope.activeBranch = branch; 
        $scope.selectedUserIndex = null;
        $scope.loadedList = '';
        $scope.hideMapButtons = true;
        

        /** Default Functions
        */
        $http.get(ctrl_path+'client_branches')
                .success(function(data){
                    if(typeof(data) == 'object' && Object.keys(data).length > 0)$scope.branches = data;
        })
            .error(function(){$scope.fail('Network error')});

        $http.get(ctrl_path+'status_count')
            .success(function(data){
                if(typeof(data) == 'object' && Object.keys(data).length > 0)$scope.reqCounts = data;
            })
            .error(function(){$scope.fail('Network error')});



        $scope.ismenuselected = function(link){
                return ($window.location.href.indexOf(link) > -1);
        }
        
        
        if(typeof(responses) != 'undefined')$scope.responses = responses;
        $scope.lists={};




        /** Page Functions
         * 
         */

        $scope.dashboard_page = function(){

            $scope.loading = true;

            $http.get(ctrl_path+'dashboard')
                .then(
                    function(data){
                        if(typeof(data.data) == 'object' && Object.keys(data.data).length > 0)
                            $scope.dashboard = data.data;
                        $scope.charts = $scope.dashboard.charts;
                        console.log($scope.dashboard)
                    },
                    function(){
                        $scope.fail('Network error');}
                ).then(function(){
                $scope.loading=false;
            })


        }


        $scope.pending_page = function(){
        
        $http.get(ctrl_path+'pending_requests')
                .success(function(data){
                    if(typeof(data) == 'object' && data.length > 0)$scope.pendings = data;
                    $scope.loading = false;
                    $scope.loadedList = 'pendings';
        })
            .error(function(){$scope.fail('Network error');$scope.loading = false;});
         
            
            
            
           /* this.pendinglist = {
            numLoaded_:0,
            toLoad_:0
            
            getItemAtIndex: function(index){
            if(index > this.numLoaded_){
            this.fetchMoreItems_(index);
            return null;
            }
            return index;
            },
            
            getLength: function(){
            return this.numLoaded_ + 5;
            },
            fetchMoreItems_:function(index){
            
            if(this.toLoad_ < index){
            this.toLoad_ += 20;
            $timeout(angular.noop, 300).then(angular.bind(this, 
            funtion(){
            this.numLoaded_ = this.toLoad;
            }))
            }
            }
            };
        */
        
        }
        


        $scope.submitted_page = function(){

        $http.get(ctrl_path+'submitted_requests_preview')
                .success(function(data){
                    if(typeof(data) == 'object' && data.length > 0)$scope.submitteds = data;
                    $scope.loading = false;
                    $scope.loadedList = 'submitteds';
        })
            .error(function(){$scope.fail('Network error');$scope.loading = false;});

           }




	$scope.preview_page = function(id,rid){
        
        $http.get(ctrl_path+'fetch_request/'+id)
                .success(function(data){
                    if(typeof(data) == 'object' ){
                    $scope.request = data;
                    $scope.report = {};
                    $scope.report.interviewee = {};
                        if(rid > 0)
              $http.get(ctrl_path+'fetch_report/'+rid)
                .success(function(data){
                    if(typeof(data) == 'object'){
                        $scope.report = data;
                        $scope.report.color = ($scope.report.color.indexOf(',') > -1)? $scope.report.color.split(','):[$scope.report.color];
                        //if(!$scope.report.structure_1)$scope.report.structure_1 = [];
                        if(!$scope.$$phase)
                            $scope.$apply();
                        $scope.populateLists();
                    }
              })
                  .error(function(){$scope.fail('Network error')});
                      }
                      else $scope.empty('Empty result');
                      $scope.loading = false;
              })
            .error(function(){$scope.fail('Network error');$scope.loading = false;});



         
           }


        $scope.upload_page = function() {

            $scope.loading = false;
            $scope.fileName = '';
            $scope.uploadFields = window.uploadFields;
            $scope.uploadErrors = [];
            $scope.assets_path = window.assets_path;

            $scope.fileNameChanged = function (ele) {
                var files = ele.files;
                $scope.newUpload = files[0];
                //console.log($scope.newUpload)
                $scope.fileName = files.length > 0 ? files[0].name : '';
                if (!$scope.$$phase)$scope.$apply();
            }
        }



/**        Additional Functions
 * 
 */

        $scope.showMap = function(){
            $scope.mapLoad = true;
            $scope.mapLoaded();
            return $scope.repMap.show($scope,true);
        }

        $scope.mapLoaded = function(){
            //
        }

        $scope.prepMapClose = function() {
            $scope.mapLoad = false;
            $scope.previewMap = undefined;
        }

        $scope.closeMap = function(){
            $scope.prepMapClose();
            $scope.repMap.close();
        }

        $scope.excel_list = function(){
            if($scope[$scope.loadedList].length <1)
                return;
            $scope.loading = true;
            $scope.toExpList = $scope[$scope.loadedList].map(function(a){
                return a.id;
            });
            $scope.do_export('excel',$scope.toExpList,$scope);
        }

        $scope.do_excel_export = function(id){
            $scope.do_export('excel',id,$scope);
        }

        $scope.do_pdf_export = function(id){
            $scope.do_export('pdf',id,$scope);
        }


        $scope.isInterviewer = function(interviewed){
            interviewed = typeof(interviewed) != 'undefined'? interviewed:'';
            return (interviewed.toLowerCase() != 'none' && interviewed.toLowerCase() != 'customer');
        }

        $scope.hasPhone = function(interviewed){
            interviewed = typeof(interviewed) != 'undefined'? interviewed:'';
            return (interviewed.toLowerCase() != 'none');
        }

        $scope.existsSet = function(retain){
           if(!retain)$scope.report.resides = '';
            $scope.lists.customerResides = $scope.report.address_exists.toLowerCase() == 'no'?['no']:$scope.responses.customer_resides;
            $scope.residesSet(retain);
        }

        $scope.getSummary = function(){
            $scope.lists.reportSummary = $scope.reportsummary($scope.report.address_exists,$scope.report.resides,$scope.report.customer);
        }


        $scope.residesSet = function(retain){
            if(!retain)$scope.report.customer = '';
            $scope.getSummary(retain);
        }

        $scope.customerSet = function(){
            $scope.getSummary();
        }
        
        $scope.completionSet = function(){
           $scope.hasBuildingDetails = $scope.report.completion.toLowerCase() != 'bare land' ? true:false;
        }

        $scope.interviewedSet = function(retain){
            if(!retain)$scope.report.interviewee = {};
            if($scope.report.interviewed.toLowerCase()=='customer')$scope.report.interviewee.name = 'Customer';
        }

        $scope.populateLists = function(){
                $scope.existsSet(true);
                $scope.customerSet();
                $scope.completionSet();
                $scope.interviewedSet(true);
        }

        $scope.reportsummary = function(address_exists,resides,customer){
            var summary = $scope.responses.summary;
            if(typeof(address_exists) == 'undefined' || typeof(summary) == 'undefined') return [];
            switch(address_exists.toLowerCase()){
                case 'yes':
                    switch(resides.toLowerCase()){
                        case 'yes':
                            if(customer == 'company')
                                return summary['yesyescom'];
                            else
                                return summary['yesyes'];
                            break;
                        case 'no':
                            return summary['yesno'];
                            break;
                        case 'moved':
                            return summary['yesmoved'];
                            break;
                        case 'locked':
                            return summary['yeslocked'];
                            break;
                        default:
                            return [];
                            break;
                    }
                    break;
                    ;
                case 'no':
                    return summary['nono'];
                    break;
                    ;
                case 'inconclusive':
                    return summary['inconclusive'];
                    break;
                    ;
                default:
                    return [];
                break;
            }
        }

        $scope.submitReport = function(scope){
            if(!scope.reportForm.$valid)return false;
            if(scope.submitAction == '') return false;
            $scope.review(scope.submitAction);
        }


        $scope.review = function(action){
            $scope.loading = true;
            $scope.report.submit = action;
            $scope.report.request_id = $scope.request.id;

            $http({
                method:'post',
                url: ctrl_path+'submit_report',
                data: $httpParamSerializerJQLike($scope.report),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
            })
                .success(function(data){
                    $scope.loading = false;
                    if(data == 'ok'){
                        $scope.ok('Action Completed');
                        setTimeout(function(){$window.location.href='../../pending';},500)
                    }
                    else if(data == 'unsubmitted')$scope.empty('No report submitted,So cannot be edited');
                    else if(data == 'submitted')$scope.empty('Report already submitted and approved');
                    else $scope.fail('Something went wrong. Please try again, Later')
                })
                .error(function(){
                    $scope.loading = false;
                    $scope.fail('Network error')
                });
        }


        $scope.createUpload = function() {
            $scope.loading = true;
            var reader = new FileReader();
            var name = $scope.newUpload.name;
            var d = [], fieldpositions = [], notFound = [];

            reader.onload = function(e) {
                var data = e.target.result;
                var wb = XLSX.read(data, {type: 'binary'});
                //var wb = XLSX.readFile($scope.newUpload.name);
                for (i in wb.Sheets) {
                    var ws = wb.Sheets[i];
                    var json = XLSX.utils.sheet_to_json(ws, {header: 1,raw:false});
                    d.push(json);
                }

                var sheet = 0;
                d.forEach(function(s){
                    fieldpositions[sheet] = [];
                    var lowerHeader = s[0].map(function(value) {
                        return value.toLowerCase();
                    });
                    $scope.uploadFields.forEach(function(fld){
                        if(lowerHeader.indexOf(fld.toLowerCase()) < 0)
                            notFound.push(fld);
                        fieldpositions[sheet][fld.toLowerCase()] = lowerHeader.indexOf(fld.toLowerCase());
                    });
                    lowerHeader.forEach(function(f){
                        if(fieldpositions[sheet].indexOf(f) < 0)
                            fieldpositions[sheet][f] = lowerHeader.indexOf(f);
                    })
                    sheet++;
                });
                if(notFound.length>0){
                    $scope.fail('Required Fields are not present['+notFound.join(',')+']');
                    $scope.loading = false;
                    return false;
                }

                var sheet = 0;var form = [];
                d.forEach(function(s){
                    for(p=1;p<s.length;p++){
                        var row = {};
                        for(i in fieldpositions[sheet]){
                            row[i] = s[p][fieldpositions[sheet][i]];
                        }
                        form.push(row)
                    }
                    sheet++;
                });

                if(form.length<1){
                    $scope.fail('The File is Empty');
                    $scope.loading = false;
                    return false;
                }

                var postData = {'requests':form};
                $http({
                    method:'post',
                    url: ctrl_path+'new_uploads',
                    data: $httpParamSerializerJQLike(postData),
                    headers : { 'Content-Type': 'application/x-www-form-urlencoded'}
                })
                    .success(function(data){
                        if(typeof(data) == 'string') {
                            $scope[data]();
                            setTimeout(function(){window.location.reload()},500);
                        }
                        else{
                            $scope.info('New Requests created. but with some errors');
                            $scope.uploadErrors = data;
                        }
                        })
                    .error(function(error){
                            $scope.fail(error);
                        })
                    .then(function(){
                        $scope.loading = false;
                    });

            }
            reader.readAsBinaryString($scope.newUpload);
        }

        $scope.updateActiveBranch = function(){
            if($scope.activeBranch == branch)
                return;
            //console.log($scope.assignedClient);
            var branchdata = new Object;
            branchdata.branch = $scope.activeBranch.id;

            $http({
                method:'post',
                url: ctrl_path+'change_active_branch',
                data: $httpParamSerializerJQLike(branchdata),
                headers : { 'Content-Type': 'application/x-www-form-urlencoded' }
                })
                        .success(function(data){
                            if(data == 'ok'){
                          $scope.ok('Change effected');
                          setTimeout(function(){$window.location.reload();},500)}
                          else $scope.empty('You have no access to that branch. Please choose carefully.')
                })
                .error(function(){
                   $scope.fail('Network error') 
                });
        }

        
       $scope.selectUserIndex = function (index) {
            if ($scope.selectedUserIndex !== index) {
              $scope.selectedUserIndex = index;
            }
            else {
              $scope.selectedUserIndex = undefined;
            }
        };
    
    
        $scope.report_icon = function(s,status){
            var v;s = parseInt(s);
        if(s == 4 && status != 'undefined')
            return(status == 'positive')?'plus-circle':'minus-circle';
        else{
        	switch (true){
        		case(s<2):
                            v = 'comment-processing-outline';break;
        		case (s===2)://console.log(2);
                            v = 'account-plus';break;
        		case (s===3)://console.log(3);
                            v = 'account-check';break;
        		case (s===4)://console.log(4);
                            v = 'comment-check-outline';break;
        		case (s===5)://console.log(5);
                            v = 'call-missed';break;
                        default:
                            v = 'adjust';break;
            }
            return v;
        	}
       	}

        $scope.getTNumbers = function(number){
            return number.split(',');
        }
                
                
}]);
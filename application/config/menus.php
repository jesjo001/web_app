<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

$config['appmenus']['admin'] = array(
	1=>array('name'=>'Upload','link'=>'Admin/upload','icon'=>'plus'),
    2=>array('name'=>'Pending','link'=>'Admin/pending','icon'=>"comment-processing-outline"),
    3=>array('name'=>'Submitted','link'=>'Admin/submitted','icon'=>"comment-text-outline"),
    4=>array('name'=>'Approved','link'=>'Admin/approved','icon'=>'comment-check-outline'),
    
);

$config['appmenus']['client'] = array(
	1=>array('name'=>'Upload','link'=>'Client/upload','icon'=>'plus'),
	2=>array('name'=>'Pending','link'=>'Client/pending','icon'=>"comment-processing-outline"),
	3=>array('name'=>'Submitted','link'=>'Client/submitted','icon'=>"comment-check-outline"),

);

$config['appmenus']['clientuser'] = array(
	1=>array('name'=>'Upload','link'=>'Clientuser/upload','icon'=>'plus'),
	2=>array('name'=>'Pending','link'=>'Clientuser/pending','icon'=>"comment-processing-outline"),
	3=>array('name'=>'Submitted','link'=>'Clientuser/submitted','icon'=>"comment-text-outline"),

);
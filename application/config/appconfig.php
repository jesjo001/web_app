<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee *
 */

/**
 * Contains config on the app
 */


/**
 * Basic running configs.
 */

/**
 * If Only the dispatch can submit reports
 */
$config['onlydispatch'] = 0;

/**
 * Header to identify mobile apps
 */
$config['app_header'] = 'avrApp';

/**
 * User groups with  access to the mobile App
 */
$config['app_access'] = ['dispatchs'];

/**
 * Maps config
 */
$config['api_key'] = [
    'maps'=>'AIzaSyCKcyz1ZUG3dBsAOG7DCjO6fNTAqTs3IFk'
];

/**
 * Users configuration
 */
$config['users'] = array(
  
);

/**
 * Licensed Company details
 */
$config['company'] = [
    'logo' => 'logo.png',
	'name' => 'Verificar Compliance',
    'address'=>'49 Lawson Street off Moloney, Lagos Island, Lagos State. 070657500400, 07031391340, 09036357373 ',

];

/**
 * User groups Config
 */
$config['groups']['dispatchs_group']						= 2;
$config['groups']['admins_group']							= 3;



/**
 * Fields for New Request Upload
 */
$config['upload_fields'] = array(
    'branch_id','customer_id','name','address','landmark','request_date','telephone'
);

/**
 * Report Responses
 * Restricted characters- :
 */

$config['report_responses'] = array(
    'address_exists'=> array('yes','no','inconclusive'),
    'customer_resides' => array('yes','no','moved','locked'),
    'summary' => array(
        'yesyes'=>array(
            'Address Exists, Customer resides',
            'Address Exists as Shack, Customer resides',
            'Address Exists, Customer resides(No direct access)',
            'Address Exists as a company, Customer is a staff',
            'Address Exists as a company, Customer works at another branch'),
        'yesyescom'=>array(
            'Address Exists, Business operates from Address',
            'Address Exists, Company Known'),
        'yesno'=>array(
            'Address Exists as Family house, Customer known but does not reside',
            'Address Exists as Demolished building, Customer known',
            'Address Exists as Bare/Empty Land, Customer known',
            'Address Exists, Customer does not reside but is known',
            'Address Exists, Customer does not reside(No direct access)',
            'Address Exists, Customer Unknown',
            'Address Exists as Demolished building, Customer not known',
            'Address Exists as Bare/Empty Land, Customer not known',
            'Address Exists , Customer Resides on different street',
            'Address Exists , Customer Resides at different number on same street',
            'Address Exists , Customer Resides at un registered house on same street',
            'Address Exists as a company, Customer is not a staff',),
        'yesmoved'=>array(
            'Address Exists, Customer moved',
            'Address Exists as a company, Customer no longer a staff',),
        'yeslocked'=>array(
            'Address Exists, under lock',
            'Address Exists, Unable to conduct interview',
            'Address Exists, Under Lockdown'),
        'nono'=>array(
            'Address does not Exist',
            'Address could not be located (No direct Access)'),
        'inconclusive'=>array(
            'Address Incomplete',
            'Address could not be located, Additional information required',
            'Street inaccessible due to Natural/Human disaster',
            'Street inaccessible due to Security Protocol')
        ),
    'customer'=>array(
        'female','male','company'
        ),
    'completion'=>array(
        'bare land','temporary','uncompleted','plastered','completed'
        ),
    'finish'=>array(
        'ceramic','glass','paint','plaster','wood','wallpaper'
    ),
    'color'=>array(
        ['name'=>'white','color'=>'#fff'],
        ['name'=>'red','color'=>'#f00'],
        ['name'=>"silver",'color'=>"#c0c0c0"],
        ['name'=>"wine",'color'=>"#722f37"],
        ['name'=>"Brown",'color'=>"#a52a2a"],
        ['name'=>"coral",'color'=>"#ff7f50"],
        ['name'=>"Orange",'color'=>"#ffa500"],
        ['name'=>"gold",'color'=>"#ffd700"],
        ['name'=>"Yellow",'color'=>"#ffff00"],
        ['name'=>"Cream",'color'=>"#f2e69b"],
        ['name'=>"Green",'color'=>"#008000"],
        ['name'=>"lemon green",'color'=>"#b0fc23"],
        ['name'=>"lightgreen",'color'=>"#90ee90"],
        ['name'=>"Blue",'color'=>"#0000ff"],
        ['name'=>"skyblue",'color'=>"#7ec0ee"],

    ),
    'type'=>array(
        array('bungalow','one story','two storey','low rise','mid rise','high rise'),
        array('attached','semi detached','detached','terraced','movable','others'),
        array('single unit','multiple unit'),
    ),
    'ownership'=>array(
        'landlord','relation','tenant','others'
    ),
    'area'=>array(
        'low','medium','high'
    ),
    'gender'=>array(
        'male','female'
    ),
    'interviewed' => array(
        "none","customer","others"
    ),
);


$config['pdf_template'] = '
                    <div style="__page_break " id="">
                    
                    <table border="1" style="width: 100%;font-size:0.9em;line-height: 24px;margin-top:0px;border:1px solid;border-color: #ddd !important;table-layout: fixed;border-collapse: collapse;">

                    <!--Header -->
                    <tr style="border:0px solid;" ><td style="height:95px;">
                    <table border="0" style="width:100%;margin-top:15px;margin-bottom:30px;"><tr>
                    <td style="width:25%" align="center"><img style="max-height:60px;max-width:100%;margin:0 auto;display:block;" src="__logosrc"></td>
                    <td style="width:50%;" align="center"><p align="center" style="font-size:0.7em"><span style="font-weight:bold">__licensed</span><br>__compaddress</p></td>
                    <td style="width:25%;" align="center"><img style="max-height:60px;max-width:100%;margin:0 auto;display:block;" src="__clilogosrc"></td>
                    </tr></table>
                    </td></tr>
                    <!--Body-->
                    
                    <tr style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;font-size:1em;font-weight:bold;font-family:Verdana" >
					<td style="text-align:center;">ADDRESS VERIFICATON REPORT FORM</td></tr>
					
                    <tr style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;" align="left"><td style="font-size:0.8em;font-weight:bold;font-family:Segoe UI;padding-left:7px;">Request Details</td></tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;" align="left">
					<table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:33%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Branch ID:</span><span>__branch_id</span></td>
                    <td style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:33%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Customer\'s ID:</span><span>__customer_id</span></td>
                    <td style="border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:34%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Date:</span><span>__request_date</span></td>
					</tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:60%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Full Name:</span><span>__name</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width:0px;width:40%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Phone Number(s):</span><span>__telephone</span></td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;min-height: 40px;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:50%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Address:</span><span>__address</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:50%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Nearest B/Stop or Landmark:</span><span>__landmark</span></td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr>
                    <td style="display:block;height:7px"></td>
					</tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;padding-left:7px;" align="left"><td style="font-size:0.8em;font-weight:bold;font-family:Segoe UI;">Report Details</td></tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;padding-left:7px;" align="left"><td style="font-size:0.6em;font-family:Verdana;"><span style="font-weight:bold;font-family:calibri">Summary:</span><span>__summary</span></td></tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:40%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Address Exists:</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Yes:</span>__address_exists:yes</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">No:</span>__address_exists:no</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Inconclusive:</span>__address_exists:inconclusive</td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:25%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Customer Resides:</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:18.75%;padding-left:7px;"><span style="font-family:calibri">Yes:</span>__resides:yes</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:18.75%;padding-left:7px;"><span style="font-family:calibri">No:</span>__resides:no</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:18.75%;padding-left:7px;"><span style="font-family:calibri">Moved:</span>__resides:moved</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:18.75%;padding-left:7px;"><span style="font-family:calibri">Locked:</span>__resides:locked</td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;padding-left:7px;" align="left"><td style="font-size:0.6em;font-family:Verdana;"><span style="font-weight:bold;font-family:calibri">Description:</span><span>__description</span></td></tr>
                    
                    <tr>
                    <td style="display:block;height:7px"></td>
					</tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;padding-left:7px;" align="left"><td style="font-size:0.9em;font-weight:bold;font-family:Segoe UI;">Building Details</td></tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:15px;padding-left:7px;"></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:24%;padding-left:7px;"><span style="font-family:calibri">Bare Land:</span>__completion:bare_land</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:19%;padding-left:7px;"><span style="font-family:calibri">Temporary:</span>__completion:temporary</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:19%;padding-left:7px;"><span style="font-family:calibri">Uncompleted:</span>__completion:uncompleted</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:19%;padding-left:7px;"><span style="font-family:calibri">Plastered:</span>__completion:plastered</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:17%;padding-left:7px;"><span style="font-family:calibri">Completed:</span>__completion:completed</td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:15px;padding-left:7px;"></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:15%;padding-left:7px;"><span style="font-family:calibri">Bungalow:</span>__structure_0:bungalow</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:15%;padding-left:7px;"><span style="font-family:calibri">One Storey:</span>__structure_0:one_storey</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:15%;padding-left:7px;"><span style="font-family:calibri">Two Storeys:</span>__structure_0:two_storey</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:15%;padding-left:7px;"><span style="font-family:calibri">Low Rise:</span>__structure_0:low_rise</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:15%;padding-left:7px;"><span style="font-family:calibri">Mid Rise:</span>__structure_0:mid_rise</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:23%;padding-left:7px;"><span style="font-family:calibri">High Rise:</span>__structure_0:high_rise</td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:15px;padding-left:7px;"></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:15%;padding-left:7px;"><span style="font-family:calibri">Attached:</span>__structure_1:attached</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:15%;padding-left:7px;"><span style="font-family:calibri">Semi Detached:</span>__structure_1:semi_detached</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:15%;padding-left:7px;"><span style="font-family:calibri">Detached:</span>__structure_1:detached</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:15%;padding-left:7px;"><span style="font-family:calibri">Terraced:</span>__structure_1:terraced</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:15%;padding-left:7px;"><span style="font-family:calibri">Movable:</span>__structure_1:movable</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:23%;padding-left:7px;"><span style="font-family:calibri">Others:</span>__structure_1:others</td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:15px;padding-left:7px;"></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:49.8%;padding-left:7px;"><span style="font-family:calibri">Single Unit:</span>__structure_2:single_unit</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:48.5%;padding-left:7px;"><span style="font-family:calibri">Multiple Units:</span>__structure_2:multiple_unit</td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:12%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Finishing:</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:10.6%;padding-left:7px;"><span style="font-family:calibri">Ceramic:</span>__finish:ceramic</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:10.6%;padding-left:7px;"><span style="font-family:calibri">Glass:</span>__finish:glass</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:10.6%;padding-left:7px;"><span style="font-family:calibri">Paint:</span>__finish:paint</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:10.6%;padding-left:7px;"><span style="font-family:calibri">Plaster:</span>__finish:plaster</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:10.6%;padding-left:7px;"><span style="font-family:calibri">Wallpaper:</span>__finish:wallpaper</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:10.6%;padding-left:7px;"><span style="font-family:calibri">Wood:</span>__finish:wood</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:24.4%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Color:</span><span>__color</span></td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:23%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Area Profile:</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:23%;padding-left:7px;"><span style="font-family:calibri">Low:</span>__area:low</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:23%;padding-left:7px;"><span style="font-family:calibri">Medium:</span>__area:medium</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:31%;padding-left:7px;"><span style="font-family:calibri">High:</span>__area:high</td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:20%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Ownership:</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Landlord:</span>__owner:landlord</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Relation:</span>__owner:relation</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Tenant:</span>__owner:tenant</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:20%;padding-left:7px;"><span style="font-family:calibri">Other:</span>__owner:other</td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr>
                    <td style="display:block;height:7px"></td>
					</tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;padding-left:7px;" align="left"><td style="font-size:0.8em;font-weight:bold;font-family:Segoe UI;">Interviewee Details</td></tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:23%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Individual Interviewed:</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:23%;padding-left:7px;"><span style="font-family:calibri">Customer:</span>__interviewed:customer</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:23%;padding-left:7px;"><span style="font-family:calibri">Others:</span>__interviewed:others</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:31%;padding-left:7px;"><span style="font-family:calibri">None:</span>__interviewed:none</td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:20%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">If Other,</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:40%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Full Name:</span><span>__interviewee_name</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:40%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Relationship:</span><span>__interviewee_relationship</span></td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;padding-left:7px;" align="left"><td style="font-size:0.6em;font-family:Verdana;"><span style="font-weight:bold;font-family:calibri">Address:</span><span>__interviewee_address</span></td></tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:50%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Telephone:</span><span>__interviewee_phone</span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:48.3%%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Gender:</span><span>__interviewee_gender</span></td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    <tr>
                    <td style="display:block;height:7px"></td>
					</tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;padding-left:7px;" align="left"><td style="font-size:0.6em;font-family:Verdana;"><span style="font-weight:bold;font-family:calibri">GPS CO-ORDINATES:</span><span>__gps</span></td></tr>
                    
                    <tr>
                    <td style="display:block;height:7px"></td>
					</tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;padding-left:7px;" align="left"><td style="font-size:0.6em;font-family:Verdana;"><span style="font-weight:bold;font-family:calibri">Cerification:</span>
                    I hereby certify that I have visited the above address of the above named customer and the information presented in this form is true and accurate to the best of my abilities</td></tr>
                    
                    <tr style="border:1px solid;border-color: #ddd !important;" >
                    <td style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;" align="left">
                    <table style="width:100%;line-height: 1.4em;font-size:0.7em;font-family:Verdana;border-collapse: collapse;"><tr>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-left-width: 0 !important;width:10%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Verifier: </span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:30%;padding-left:7px;">__licensed TEAM</td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;width:25%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Sign:</span><span><img style="max-height:30px;max-width:100%;margin:0 auto;display:inline-block;" src="__signsrc"></span></td>
                    <td style="float:left;border:1px solid;border-color: #ddd !important;border-bottom-width: 0 !important;border-top-width: 0 !important;border-right-width: 0 !important;width:31.38%;padding-left:7px;"><span style="font-weight:bold;font-family:calibri">Date:</span><span>__date_approved</span></td>
                    </tr></table>
                    </td>
                    </tr>
                    
                    </table>
                    
                    </div>
                    ';
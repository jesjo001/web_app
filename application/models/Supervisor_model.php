<?php

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

class Supervisor_model extends CI_Model{
    
    public function fetch_assigned_clients($user_id,$no = 0)
    {
        $sql = "SELECT c.name,c.id FROM companies c INNER JOIN user_clients uc on uc.company_id = c.id WHERE uc.user_id = $user_id";
        if($no>0)$sql .= " limit $no";
        $result = $this->db->query($sql);
        $clients = array();
        foreach($result->result() as $row){
           $clients[] = array('name' => $row->name,'id'=>$row->id); 
        }
        return ($no == 1)? array_shift($clients):$clients;
    }
    
    
    
}
<?php
/**
 * Copyright (c) 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 10/2016
 * Licensed to: LicensedTo
 * License subject to changes based on agreement between  Author and Licensee
 */

class Client_model extends CI_Model{



    /*public function fetch_branch_dispatchers($bid){
        $sql = "SELECT b.name,b.id FROM branches b INNER JOIN user_clients uc on uc.branch_id = b.id WHERE uc.user_id = $user_id";
        if($no>0)$sql .= " limit $no";
        $result = $this->db->query($sql);
        $clients = array();
        foreach($result->result() as $row){
           $clients[] = array('name' => $row->name,'id'=>$row->id); 
        }
        return ($no == 1)? array_shift($clients):$clients;
    
    }*/



    /**
     * Return clients assigned to admin;delimeted by number of clients to return.
     * @param type $user_id User id of Amin to fetch clients for
     * @param type $no Number of clients to fetch.Returns all if not set
     * @return array Client(s) assigned to Admin.
     */
    public function fetch_client_branches($user_id=0,$no = 0)
    {
        $sql = "SELECT b.name,b.id FROM branches b 
                INNER JOIN users_branch ub on ub.branch_id = b.id 
                WHERE ub.user_id = $user_id";
        if($no>0)$sql .= " limit $no";
        $result = $this->db->query($sql);
        $clients = array();
        foreach($result->result() as $row){
            $clients[] = array('name' => $row->name,'id'=>$row->id);
        }
        return ($no == 1)? array_shift($clients):$clients;
    }


    public function fetch_assigned_clients_by_id($user_id=0,$branch_id=0,$no = 0)
    {
        $sql = "SELECT b.name,b.id FROM branches b INNER JOIN user_clients uc on uc.branch_id = b.id WHERE uc.user_id = $user_id and b.id=$branch_id";
        if($no>0)$sql .= " limit $no";
        $result = $this->db->query($sql);
        $clients = array();
        foreach($result->result() as $row){
            $clients[] = array('name' => $row->name,'id'=>$row->id);
        }
        return ($no == 1)? array_shift($clients):$clients;
    }


}
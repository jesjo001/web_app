<?php

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

class Admin_model extends CI_Model{
    
    
    
    /*public function fetch_branch_dispatchers($bid){
        $sql = "SELECT b.name,b.id FROM branches b INNER JOIN user_clients uc on uc.branch_id = b.id WHERE uc.user_id = $user_id";
        if($no>0)$sql .= " limit $no";
        $result = $this->db->query($sql);
        $clients = array();
        foreach($result->result() as $row){
           $clients[] = array('name' => $row->name,'id'=>$row->id); 
        }
        return ($no == 1)? array_shift($clients):$clients;
    
    }*/
    
    
    
    /**
     * Return clients assigned to admin;delimeted by number of clients to return.
     * @param type $user_id User id of Amin to fetch clients for
     * @param type $no Number of clients to fetch.Returns all if not set
     * @return array Client(s) assigned to Admin. 
     */
    public function fetch_assigned_clients($user_id=0,$no = 0)
    {
        $sql = "SELECT b.name,b.id,c.name company_name FROM branches b INNER JOIN user_clients uc on uc.branch_id = b.id INNER JOIN companies c on b.company_id = c.id WHERE uc.user_id = $user_id";
        if($no>0)$sql .= " limit $no";
        $result = $this->db->query($sql);
        $clients = array();
        foreach($result->result() as $row){
           $clients[] = array('name' => $row->name,'id'=>$row->id, 'company'=>$row->company_name);
        }
        return ($no == 1)? array_shift($clients):$clients;
    }
    
    
    public function fetch_assigned_clients_by_id($user_id=0,$branch_id=0,$no = 0)
    {
        $sql = "SELECT b.name,b.id,c.name company_name FROM branches b INNER JOIN user_clients uc on uc.branch_id = b.id INNER JOIN companies c on b.company_id = c.id WHERE uc.user_id = $user_id and b.id=$branch_id";
        if($no>0)$sql .= " limit $no";
        $result = $this->db->query($sql);
        $clients = array();
        foreach($result->result() as $row){
            $clients[] = array('name' => $row->name,'id'=>$row->id, 'company'=>$row->company_name);
        }
        return ($no == 1)? array_shift($clients):$clients;
    }
    
    
}
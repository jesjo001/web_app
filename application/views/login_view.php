<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

?>
<title>Portal Access </title>

<div flex layout="row" layout-align="center center" layout-padding>
    <div flex-xs="100" flex-gt-xs="70" flex-gt-sm="50" class="md-whiteframe-z1">
        
<!-- Container #1 (see wireframe) -->
    <md-toolbar layout="row" flex layout-align="center" >
      <h1>Login</h1>
    </md-toolbar>
    <md-progress-linear ng-show="loading" md-mode="query"></md-progress-linear>

    

        <!-- Container #4 -->
        <md-content layout-margin  >
            
       <form name='logon' layout="column" ng-submit="doLogin()"> 
 
            <md-input-container >
            <label>Username</label>
            <input type="text" name="uname" ng-model="loginFormdata.uname" required>
            </md-input-container>
            
            <md-input-container >
            <label>Password</label>
            <input type="password" ng-model="loginFormdata.pass" required>
            </md-input-container>

            <md-checkbox ng-model="loginFormdata.remember" aria-label="Remember me ?">
            Remember me ?
            </md-checkbox>

            <div layout="row" layout-align="center">
            <md-button ng-disabled="!(logon.$valid && logon.$dirty) || notSubmit" 
                       class="md-fab md-raised md-hue-1" md-padding aria-label="Login" type="submit" >
                <md-tooltip>
                  Login
                </md-tooltip>

            <md-icon md-svg-icon="login"></md-icon>

  					 </md-button>
            </div>

</form>
            
        </md-content>

    </div>
    </div>
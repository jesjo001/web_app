<?php

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

?>

<script>
    responses = <?php echo json_encode($responses)?>;
</script>

<title data-ng-init="preview_page(<?php echo $id ?>,<?php echo $rid ?>)" >{{request.name}}</title>

<md-card layout="column" ng-if="request" flex="100" flex >
    <md-content flex="100"  layout="row" layout-wrap  layout-padding>
        <md-card-header flex="100" layout="row">
            <md-card-header-text  flex>
                <md-title>
                    <md-icon md-svg-src="{{report_icon(request.status,request.report_status)}}" class="col_{{request.report_status}}"></md-icon>
                    <b>{{request.name}}</b>
                </md-title>
            </md-card-header-text>
        </md-card-header>

        <md-card-title layout="row" flex="100">
            <md-card-title-text layout="row" layout-wrap flex="100">
                <md-headline flex="100" layout-padding>
                    <label>Address:</label>
                    {{request.address}}
                </md-headline>
                <md-subhead flex="100" layout-padding>
                    <label>Landmark:</label>
                    {{request.landmark}}
                </md-subhead>

                <md-menu ng-if="request.telephone" flex="100">
                    <md-button ng-click="$mdOpenMenu($event)">View Phone numbers</md-button>
                    <md-menu-content layout="column">
                        <a ng-repeat="t in getTNumbers(request.telephone)" flex layout-margin href="tel:{{t}}">{{t}} <md-divider></md-divider></a>
                    </md-menu-content>
                </md-menu>

                <div layout="row" layout-padding  flex="50" flex-xs="100" layout-wrap>
                    <md-input-container  flex="50" flex-xs="100">
                        <label>-- Dispatch --</label>
                        <md-select aria-label="Assign dispatch" ng-model="request.dispatch_id" ng-change="update_dispatch(request.id,request.dispatch_id)" ng-disabled="(request.status > 2)" placeholder=" -- Assign Dispatch-- ">
                            <md-option value="0"> None </md-option>
                            <md-option value="{{d.id}}" ng-repeat="d in dispatchers">{{d.username}}</md-option>
                        </md-select>
                    </md-input-container>

                    <md-input-container flex-sm="50" flex-xs="100" flex-gt-sm="25">
                        <label> -- Admin -- </label>
                        <input aria-label="Admin Staff" type='text' placeholder="None" value="{{admins_name(report.admin_id)}}" ng-disabled = "1" />
                    </md-input-container>

                    <div layout="row" flex="100">
                        <md-button ng-disabled="!report.gps" ng-click="showMap()">
                            <md-icon md-svg-icon="map"></md-icon>
                            Location
                        </md-button>
                    </div>
                </div>

                <div layout="column"   flex="50" flex-xs="100">
                    <p layout="row" layout-wrap flex layout-padding>
                        <label flex-xs="50" flex>Created:</label>
                        <span flex>{{request.date_created}}</span>
                    </p>
                    <p layout="row" layout-wrap flex layout-padding>
                        <label flex-xs="50" flex>Assigned:</label>
                        <span flex>{{request.dispatch.time}}</span>
                    </p>
                    <p layout="row" layout-wrap flex layout-padding>
                        <label flex-xs="50" flex>Submitted:</label>
                        <span flex>{{report.date_created}}</span>
                    </p>
                    <p layout="row" layout-wrap flex layout-padding>
                        <label flex-xs="50" flex>Approved:</label>
                        <span flex>{{report.date_approved}}</span>
                    </p>
                </div>
            </md-card-title-text>
        </md-card-title>

        <md-card-content flex="100" >
            <form name="reportForm" id="rform" ng-submit="submitReport(this)" novalidate layout="row" layout-wrap >

                <label flex="100" style="margin-top:20px;">Report</label>


                <var flex="100"  ng-if="nosubmit" ><md-icon md-svg-src="caution" class="warn"></md-icon> Dispatch is yet to submit a report. Wait for report from dispatch to approve.</var>

                <div flex="100" layout="row" layout-padding layout-wrap>
                    <md-input-container flex-gt-xs="50" flex="100">
                        <label>Address Exists</label>
                        <md-select ng-model="report.address_exists" ng-disabled="request.status > 3" ng-change="existsSet()" ng-required="true">
                            <md-option></md-option>
                            <md-option  ng-repeat="e in responses.address_exists" value="{{::e}}">{{::e | uppercase}}</md-option>
                        </md-select>
                    </md-input-container>

                    <md-input-container flex=-gt-xs"50" flex="100" ng-if="report.address_exists!='inconclusive'">
                        <label>Customer Resides</label>
                        <md-select ng-model="report.resides" ng-disabled="request.status > 3" ng-change="residesSet()"  ng-required="true">
                            <md-option></md-option>
                            <md-option ng-repeat="r in lists.customerResides" ng-value="::r">{{::r | uppercase}}</md-option>
                        </md-select>
                    </md-input-container>

                    <md-input-container flex="100" ng-if="report.address_exists!='inconclusive' && (report.resides!='no' && report.resides!='locked')">
                        <label>Customer</label>
                        <md-select ng-model="report.customer" ng-disabled="request.status > 3" ng-change="customerSet()" ng-required="true">
                            <md-option></md-option>
                            <md-option ng-repeat="cu in responses.customer" ng-value="::cu">{{::cu | uppercase}}</md-option>
                        </md-select>
                    </md-input-container>

                    <md-input-container flex="100">
                        <label>Report summary</label>
                        <md-select ng-model="report.summary" ng-disabled="request.status > 3" ng-required="true">
                            <md-option ng-repeat="s in lists.reportSummary" ng-value="::s" >{{::s | uppercase}}</md-option>
                        </md-select>
                    </md-input-container>

                    <md-input-container flex="100" >
                        <label>Description</label>
                        <textarea ng-model="report.description" ng-disabled="request.status > 3" ng-required="report.resides.toLowerCase()!='yes'"></textarea>
                    </md-input-container>
                </div>

                <div ng-if="report.address_exists.toLowerCase() == 'yes'" flex layout="row" layout-wrap layout-padding>

                    <label flex="100">Building Details</label>

                    <md-input-container flex="100">
                        <label>Completion</label>
                        <md-select ng-model="report.completion" ng-disabled="request.status > 3" ng-change="completionSet()" ng-required="true">
                            <md-option></md-option>
                            <md-option  ng-repeat="c in responses.completion" value="{{::c}}">{{::c | uppercase}}</md-option>
                        </md-select>
                    </md-input-container>

                    <div ng-if="hasBuildingDetails" flex="100" layout-padding layout="row" layout-wrap>

                        <md-input-container ng-if="report.completion=='completed'" flex-gt-xs="50" flex="100">
                            <label>Finishing</label>
                            <md-select ng-model="report.finish" ng-disabled="request.status > 3" ng-change="finishSet()" ng-required="true">
                                <md-option></md-option>

                                <md-option ng-repeat="f in responses.finish" ng-value="::f">{{::f | uppercase}}</md-option>
                            </md-select>
                        </md-input-container>

                        <md-input-container ng-if="report.finish=='paint'" flex-gt-xs="50" flex="100">
                            <label>Colors</label>
                            <md-select ng-model="report.color" ng-disabled="request.status > 3" ng-required="true" multiple="true" >
                                <md-option></md-option>
                                <md-option ng-repeat="co in responses.color" style="background-color:{{co.color}}" ng-value="::co.name">{{::co.name | uppercase}}</md-option>
                            </md-select>
                        </md-input-container>

                    </div>

                    <div flex="100" layout="row" layout-padding layout-wrap>

                        <md-input-container flex-gt-xs="50" flex="100">
                            <label>Ownership</label>
                            <md-select ng-model="report.owner" ng-disabled="request.status > 3"  ng-required="true">
                                <md-option></md-option>

                                <md-option ng-repeat="o in responses.ownership" ng-value="::o">{{::o | uppercase}}</md-option>
                            </md-select>
                        </md-input-container>

                        <md-input-container flex-gt-xs="50" flex="100">
                            <label>Area Profile</label>
                            <md-select ng-model="report.area" ng-disabled="request.status > 3" ng-required="true">
                                <md-option></md-option>
                                <md-option ng-repeat="a in responses.area" ng-value="::a">{{::a | uppercase}}</md-option>
                            </md-select>
                        </md-input-container>

                    </div>


                    <div ng-if="report.completion!='bare land'" flex="100" layout="row"  layout-wrap layout-padding>

                        <label flex="100">Structure</label>

                        <md-list layout-padding flex="100">
                            <md-list-item >
                                <md-radio-group flex="100" layout-padding ng-model="report.structure_0" ng-required="true" layout="row" layout-wrap  md-colors="{'border-color':'warn'}" >
                                    <md-radio-button ng-repeat="s0 in responses.type[0]"
                                                     ng-value="s0"
                                                     ng-disabled="request.status > 3"
                                    >
                                        {{s0 | uppercase}}
                                    </md-radio-button>
                                </md-radio-group>

                                <md-divider ></md-divider>
                            </md-list-item>

                            <md-list-item flex="100" >
                                <md-radio-group layout-padding ng-model="report.structure_1" ng-required="true" layout="row" layout-wrap  md-colors="{'border-color':'warn'}" >
                                    <md-radio-button ng-repeat="s1 in responses.type[1]"
                                                     ng-value="s1"
                                                     ng-disabled="request.status > 3"
                                    >
                                        {{s1 | uppercase}}
                                    </md-radio-button>
                                </md-radio-group>

                                <md-divider ></md-divider>
                            </md-list-item>

                            <md-list-item flex="100">
                                <md-radio-group flex="100" layout-padding ng-model="report.structure_2" ng-required="true" layout="row" layout-wrap  md-colors="{'border-color':'warn'}" >
                                    <md-radio-button ng-repeat="s2 in responses.type[2]"
                                                     ng-value="s2"
                                                     ng-disabled="request.status > 3"
                                    >
                                        {{s2 | uppercase}}
                                    </md-radio-button>
                                </md-radio-group>

                                <md-divider ></md-divider>
                            </md-list-item>

                        </md-list>

                    </div>

                </div>

                <div flex="100" layout="row" layout-wrap layout-padding>

                    <label flex="100">Interviewee Details</label>

                    <md-radio-group flex="100" layout-padding ng-model="report.interviewed" ng-required="true" ng-change="interviewedSet()" layout="row" layout-wrap layout-padding  md-colors="{'border-color':'warn'}" >
                        <md-radio-button ng-repeat="i in responses.interviewed"
                                         ng-value="i"
                                         ng-disabled="request.status > 3"
                        >
                            {{i | uppercase}}
                        </md-radio-button>
                    </md-radio-group>

                    <md-input-container ng-if="hasPhone(report.interviewed)" flex="100">
                        <label>Name</label>
                        <input ng-model="report.interviewee.name"  ng-disabled="request.status > 3 || report.interviewed.toLowerCase() == 'customer'" ng-required="true">
                    </md-input-container>

                    <md-input-container flex="100" ng-if="isInterviewer(report.interviewed)">
                        <label>Address</label>
                        <input ng-model="report.interviewee.address"  ng-disabled="request.status > 3" ng-required="true">
                    </md-input-container>

                    <md-input-container flex="100" ng-if="isInterviewer(report.interviewed)">
                        <label>Relationship</label>
                        <input ng-model="report.interviewee.relationship"  ng-disabled="request.status > 3" ng-required="true">
                    </md-input-container>


                    <div layout="row" flex="100" layout-wrap>

                        <md-input-container flex="50" flex-xs="100" ng-if="hasPhone(report.interviewed)">
                            <label>Telephone</label>
                            <input ng-model="report.interviewee.phone" type="tel"  ng-disabled="request.status > 3">
                        </md-input-container>

                        <md-input-container flex="50" flex-xs="100" ng-if="isInterviewer(report.interviewed)">
                            <label>Gender</label>
                            <md-select ng-model="report.interviewee.gender" ng-disabled="request.status > 3" ng-required="true">
                                <md-option></md-option>
                                <md-option ng-repeat="g in responses.gender" ng-value="::g">{{::g | uppercase}}</md-option>
                            </md-select>
                        </md-input-container>

                    </div>


                </div>


            </form>



        </md-card-content>

    </md-content>


    <md-fab-speed-dial md-direction="left" class="md-scale  md-fab-bottom-right" >
        <md-fab-trigger>
            <md-button aria-label="menu" class="md-fab md-accent">
                <md-icon md-svg-icon="menu"></md-icon>
            </md-button>

        </md-fab-trigger>
        <md-fab-actions >
            <md-button aria-label="Excel" ng-click="do_excel_export(request.id)" ng-disabled="loading">
                <md-tooltip>Excel</md-tooltip>
                <md-icon md-svg-icon="file-excel" md-colors="{color:loading?'grey':'green-500'}"></md-icon>
            </md-button>
            <md-button aria-label="Pdf"  ng-if="request.status > 3" ng-click="do_pdf_export(request.id)"  ng-disabled="loading">
                <md-tooltip>Pdf</md-tooltip>
                <md-icon md-svg-icon="file-pdf"  md-colors="{color:loading?'grey':'red-800'}"></md-icon>
            </md-button>
        </md-fab-actions>
    </md-fab-speed-dial>

</md-card>

<md-card ng-if="!request && !loading" flex>
    <var align='center'> Either Request does not exist or Request details not found at this time.Do something else </var>
</md-card>
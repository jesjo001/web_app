<?php
/**
 * Copyright (c) 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 9/2016
 * Licensed to: LicensedTo
 * License subject to changes based on agreement between  Author and Licensee
 */

?>

<title>Create Requests</title>

<div flex data-ng-init="upload_page()" layout="row">

    <div layout="column" flex="100">


            <md-card flex class="md-whiteframe-z1" >
                    <md-card-header layout-align="left" >
                        <md-card-header-text>
                            <span class="md-title" layout-padding>Upload New Requests</span>
                            <span class="md-subhead" layout-padding>Create new requests by uploading an excel (xls,Xlsx) file </span>
                            <span class="md-subhead" layout-padding>Download <a ng-href="{{assets_path}}templates/avr_upload_template.xlsx" class="md-accent">sample template</a> </span>
                        </md-card-header-text>
                    </md-card-header>
                    <md-card-content layout="row">
                        <div flex="100" layout-align="center" layout="column" >
                            <div class="md-caption" align="center">
                                Required Column Headers: <span ng-repeat="field in uploadFields"><a class="md-accent" >{{field}}</a>,</span>
                                <p>request_date(dd/mm/yyyy H:m:s)</p>
                            </div>
                            <form ng-submit="createUpload()" layout="row" layout-wrap layout-align="center center">

                                <input class="ng-hide" id="input-file-id" type="file" onchange="angular.element(this).scope().fileNameChanged(this)" ng-disabled="loading"/>
                                <label flex="100" for="input-file-id" class="md-button ">{{fileName == ''?'Choose File':fileName}}</label>
                            <!--<md-input-container >
                                <label for="inputfile"> Excel File  </label>
                                <input style="visibility: hidden" aria-label="File" type='file' id="inputfile" />
                            </md-input-container>-->

                            <md-button  type="submit" class="md-accent md-raised" ng-disabled="fileName=='' || loading">
                                <md-icon md-svg-icon="cloud-upload" ></md-icon>
                                <md-tooltip>Upload</md-tooltip>
                            </md-button>
                            </form>
                        </div>
                        <div ng-if="uploadErrors.length" flex="100">
                            <span class="md-subheader">Errors</span>
                            <md-list>
                                <md-list-item ng-repeat="error in uploadErrors">
                                    {{error}}
                                    </md-list-item>
                                </md-list>
                            </div>



                </md-card>
    </div>


</div>
<script>
    uploadFields = <?=json_encode($upload_fields)?>;
</script>
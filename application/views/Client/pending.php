<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

?>

<title>Pending Requests</title>

<div flex data-ng-init="pending_page()"  layout="row">
    
 <div ng-if="(pendings)" layout="column" flex="100" >

 <?php $this->load->view($role.'/layout/filter',array());?>

 <div layout="column" flex>
   <md-virtual-repeat-container flex="100">
  <md-card flex class="md-whiteframe-z1" md-virtual-repeat="pending in pendings |  filter:src:strict | orderBy:oT:oR">
  <md-card-header class="curspoint"layout-align="left" ng-click="page_change('preview/'+pending.id+'/'+pending.report_id)">
  <md-icon md-svg-src="adjust" class="col_{{pending.status}}"></md-icon>
  <md-card-header-text>
  <span class="md-title" layout-padding>{{pending.address}}</span>
  </md-card-header-text>
  </md-card-header>
  <md-card-content layout="row" layout-wrap>
    <div flex-gt-md="70" flex="100">
  <p>
  <label>Name:</label> {{pending.name}}
  </p>
  <p>
  <label>Landmark:</label>{{pending.landmark}}
  </p>
  <div>
  <md-menu ng-if="pending.telephone">
  <md-button ng-click="$mdOpenMenu($event)">View Phone numbers</md-button>
  <md-menu-content layout="column"><a ng-repeat="t in getTNumbers(pending.telephone)" flex layout-margin href="tel:{{t}}">{{t}} <md-divider></md-divider></a></md-menu-content>
  </md-menu>
  </div>
    </div>

    <div flex-gt-md="30" flex="100">
      <md-input-container >
        <md-select aria-label="Assign dispatch" ng-model="pending.dispatch_id" ng-change="update_dispatch(pending.id,pending.dispatch_id)" ng-disabled="(pending.status > 2)" placeholder=" -- Assign Dispatch-- ">
          <md-option value="0"> None </md-option>
          <md-option value="{{d.id}}" ng-repeat="d in dispatchers">{{d.username}}</md-option>
        </md-select>
      </md-input-container>
    </div>

  </md-card-content>
  <md-footer flex layout-padding>
  <md-card-actions layout="row" layout-fill >

  </md-card-actions>
  </md-footer>
  
  </md-card>
</md-virtual-repeat-container>
   </div>

</div>

<md-card ng-if="!pendings && !loading" flex layout-fill>
  <var align='center'> No requests available at this time.Do something else </var>
  </md-card>
    
</div>
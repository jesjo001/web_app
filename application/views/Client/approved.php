<?php

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

?>

<title>Approved Requests</title>

<div flex data-ng-init="approved_page()" layout="row">
    
 <div ng-if="(approveds)" layout="column" flex="100">

 <?php $this->load->view($role.'/layout/filter',array());?>

 <div layout="column" flex>
<md-virtual-repeat-container flex="100">
  <md-card flex class="md-whiteframe-z1" md-virtual-repeat="approved in approveds | filter:src:strict | orderBy:oT:oR" md-href = "/review/{{approved.id}}">
  <md-card-header layout-align="left" ng-click="page_change('preview/'+approved.id+'/'+approved.report_id)">
  <md-icon md-svg-src="{{report_icon(approved.status,approved.report_status)}}" class="col_{{approved.report.status}}"></md-icon>
  <md-card-header-text>
  <span class="md-title" layout-padding>{{approved.address}}</span>
  </md-card-header-text>
  </md-card-header>
  <md-card-content layout="row">
  <div flex="40" flex-sm >
  <md-input-container >
      <label> Name  </label>
        <input aria-label="Name" type='text' value="{{approved.name}}" ng-disabled = "1" />
  </md-input-container>
  </div>
      <div flex flex-gt-xs="25">
  <md-input-container >
      <label> -- Dispatch -- </label>
        <input aria-label="Assigned Dispatch" type='text' placeholder="None" value="{{dispatchs_name(approved.report.dispatch_id)}}" ng-disabled = "1" />
  </md-input-container>
      </div>
      <div flex flex-gt-xs="20">
  <md-input-container >
      <label> -- Admin -- </label>
        <input aria-label="Admin Staff" type='text' placeholder="None" value="{{admins_name(approved.report.admin_id)}}" ng-disabled = "1" />
  </md-input-container>
      </div>
  
  </md-card>
</md-virtual-repeat-container> 
</div>
     </div>

<md-card ng-if="!approveds  && !loading" flex>
  <var align='center'> No requests available at this time.Do something else </var>
  </md-card>
    
</div>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


/**
 * Copyright (c) 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 8/2016
 * Licensed to: LicensedTo
 * License subject to changes based on agreement between  Author and Licensee
 */

?>

<div layout="row" layout-wrap >
    <div flex="100" flex-gt-md="60" layout="row" layout-wrap>
        <md-input-container flex-gt-xs="60" flex="100">
            <input type="text" ng-model ="src[sT]" placeholder="search" aria-label="search">
        </md-input-container>

        <md-input-container flex-gt-xs="40" flex="100">
            <label>Search by</label>
            <md-select ng-model = "sT" placeholder="Search by" aria-label="search value">
                <md-option ng-repeat="s in searchtype" value="{{s.term}}">{{s.name}}</md-option>
            </md-select>
        </md-input-container>

    </div>

    <div flex="100" flex-gt-md="40" layout = "row" >
        <md-input-container flex-gt-xs="60" flex="80">
            <label>Order By </label>
            <md-select ng-model = "oT" placeholder="Order by" aria-label="order by">
                <md-option ng-repeat="t in ordertype" value="{{t.term}}" >{{t.name}}</md-option>
            </md-select>
        </md-input-container>

        <md-button class="md-icon-button" flex-gt-xs="30" flex="20"  ng-click="toggle_order()" placeholder="Order by" aria-label="order value" ng-style="{width:'2em'}">
            <md-icon md-svg-src="{{(!oR)?'arrow-up-bold':'arrow-down-bold'}}"></md-icon>
        </md-button>

        <md-button class="md-icon-button" flex-gt-xs="30" flex="20" ng-disabled="loading" ng-click="excel_list()" placeholder="Export all to excel" aria-label="Export all to excel" ng-style="{width:'2em'}">
            <md-icon md-svg-src="file-excel" md-colors="{fill:loading?'gray':'green'}" ></md-icon>
        </md-button>

    </div>
</div>

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

?>

  <!-- Angular Material requires Angular.js Libraries --
  <script src="<?php echo base_url() ?>assets/js/dependencies/angular-1.4.8.js"></script>
  <script src="<?php echo base_url() ?>assets/js/dependencies/angular-animate-1.4.8.js"></script>
  <script src="<?php echo base_url() ?>assets/js/dependencies/angular-aria-1.4.8.js"></script>
  <script src="<?php echo base_url() ?>assets/js/dependencies/angular-messages-1.4.8.js"></script>

  <!-- Angular Material Library --
  <script src="<?php echo base_url() ?>assets/js/dependencies/angular-material-1.0.0.js"></script>
  
  <!--  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>

  <!-- Angular Material Library -->
  <!--<script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>
  -->
  
  <!-- Your application bootstrap  --
  <script src="<?php echo base_url() ?>assets/js/app.js"></script>
  
  
  <script src="<?php echo $dir ?>assets/js/controller.js"></script>

  -->
  
</md-content>

</md-content>
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee *
 */

?>

  <!--pdf exporter renderer-->
  <div id="print_editor"></div>

  <!-- Angular Material requires Angular.js Libraries -->
  <script src="<?= base_url() ?>assets/js/dependencies/angular-1.5.8.min.js"></script>
  <script src="<?= base_url() ?>assets/js/dependencies/angular-animate-1.5.8.min.js"></script>
  <script src="<?= base_url() ?>assets/js/dependencies/angular-aria-1.5.8.min.js"></script>
  <script src="<?= base_url() ?>assets/js/dependencies/angular-messages-1.5.8.min.js"></script>

  <!-- Angular Material Library -->
  <script src="<?= base_url() ?>assets/js/dependencies/angular-material-1.1.1.min.js"></script>

  <!--  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-animate.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-aria.min.js"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular-messages.min.js"></script>

  <!-- Angular Material Library -->
  <!--<script src="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.js"></script>
  -->

<!-- Google Maps -->
  <script src="http://maps.google.com/maps/api/js?key=<?=$mapkey?>"></script>
  <script src="<?= base_url() ?>assets/js/dependencies/ng-map.min.js"></script>


<!--Add Excel, PDF and File related functions-->
  <script src="<?= base_url() ?>assets/js/dependencies/xlsx.core.min.js"></script>
  <script src="<?= base_url() ?>assets/js/dependencies/FileSaver.min.js"></script>
  <!--<script src="<?= base_url() ?>assets/js/dependencies/jspdf.min-stable.js"></script>-->
  <script src="<?= base_url() ?>assets/js/dependencies/jspdf.min.js"></script>
  <script src="<?= base_url() ?>assets/js/dependencies/html2pdf.js"></script>
  <!--<script src="<?= base_url() ?>assets/js/dependencies/addhtml.js"></script>-->

  <!--Chart fusion--
  <script src="<?= base_url()?>assets/js/dependencies/fusioncharts.js"></script>
  <script src="<?= base_url()?>assets/js/dependencies/fusioncharts.charts.js"></script>
  <script src="<?= base_url()?>assets/js/dependencies/angular-fusioncharts.min.js"></script>
  <script src="<?= base_url()?>assets/js/dependencies/themes/fusioncharts.theme.carbon.js"></script>
  <script src="<?= base_url()?>assets/js/dependencies/themes/fusioncharts.theme.fint.js"></script>
  <script src="<?= base_url()?>assets/js/dependencies/themes/fusioncharts.theme.ocean.js"></script>
  <script src="<?= base_url()?>assets/js/dependencies/themes/fusioncharts.theme.zune.js"></script>
  -->

  <!--Chart.js-->
  <script src="<?= base_url()?>assets/js/dependencies/Chart.min.js"></script>
  <script src="<?= base_url()?>assets/js/dependencies/angular-chart.min.js"></script>


<!--Additional functions-->
  <script src="<?= base_url() ?>assets/js/functions.js"></script>

  <!-- Your application bootstrap  -->
  <script src="<?= base_url() ?>assets/js/app.js"></script>
  <script src="<?= sprintf($dir,'assets/') ?>js/controller.js"></script>

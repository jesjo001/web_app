<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */



//initialize every required information to determine view location

$base = base_url();
$site = site_url();
$dir = $base.'%1$s'.$role;
$dir = ($role)?$dir.'/':$dir;
$appdir = ($role)?$role.'/':'';

$data['data'] = $data;
$data['role'] = $role;
$data['dir'] = $dir;
$data['base'] = $base;
$data['appdir'] = $appdir;
$data['assetdir'] = base_url('assets');

//load view

$this->load->view('layout/top.php',$data);
?>

<body ng-app="AVRapp" ng-controller="<?= $angular_controller ?>" ng-cloak layout="column" layout-fill>

<?php
if($role) $this->load->view($appdir.'layout/top.php',$data);


/*<body ng-app="AVRapp" ng-cloak layout="column">
*/
?>

<script>
    var api_path = '<?= $site?>/api/',
    //   api_path = '<?= $base?>/api/';
    ctrl_path = '<?= $site?>/<?php echo $ctrl_path?>/',
    assets_path = '<?= $base?>assets/',
    site_path = '<?= $site?>/',
    role_api_path = '<?= $base?>/<?= $role ?>/api/',
    //secureApi = 'https://localhost/wedeliver/public_html/api/'
    loc = '<?= site_url('login');?>',
    licensed = '<?=$licensed;?>'
    <?php
        if(isset($logo))
            echo ',logo = "'.$logo.'"';
        if(isset($address))
            echo ',address="'.$address.'"';
    ?>;

    </script>
     
<?php

  $this->load->view($appdir.$view,$data);


$this->load->view('layout/bottom.php',$data);

if($role) $this->load->view($appdir.'layout/bottom.php',$data);
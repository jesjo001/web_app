<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */


?>
<title>Page not Found </title>

<div flex layout="row" layout-align="center center" layout-padding>
    <div flex-xs="100" flex-gt-xs="70" flex-gt-sm="50" class="md-whiteframe-z1">
    
        <!-- Container #4 -->
        <md-content layout-margin align="center" >
            
            <img src="<?php echo $assetdir?>/img/logo.png" alt="<?php echo site_url()?>" class="logo">
            
            
                <h2>Oopssie !!!</h2>
                <h4>You must have taken a wrong turn </h4>
                <md-card flex layout="row" heigth="5em">
                    <md-button flex="50" ng-href="<?php echo $base?>"><md-icon md-svg-icon="home" ></md-icon>Home</md-button>
                    <md-button flex="50"  ng-click="logout('<?php echo $base?>')"><md-icon md-svg-icon="logout" ></md-icon>Logout</md-button>
                </md-card>
            
            
        </md-content>

    </div>
    </div>
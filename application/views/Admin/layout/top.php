<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

?>

<!--
<html lang="en" >
<head>
  <!-- Angular Material style sheet --
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dependencies/angular-material-1.0.0.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dependencies/font-awesome.min.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/dependencies/material-icons.css">
 <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/style.css">

 


 <!-- <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/angular_material/1.0.0/angular-material.min.css">
-->

<script>
 var menu = <?php echo json_encode($menu)?>;
 var client = <?php echo json_encode($client)?>;
 var onlydispatch = <?php echo ($onlydispatch)?'true':'false'?>;
</script>

<md-toolbar layout='row' class="md-whiteframe-z4">
    <div class="md-toolbar-tools">
        
            <!-- Trigger element is a md-button with an icon -->
            <md-button ng-click="toggleLeft()" class="md-icon-button md-button md-ink-ripple" aria-label="Open menu" type='button'>
              <md-icon md-colors="{fill:'accent'}" md-svg-icon="menu" class="ng-scope ng-isolate-scope" aria-hidden="true"></md-icon>
              <md-tooltip>
                  Left Menu
              </md-tooltip>
            </md-button>
            
            
          <h3 ><?php echo $company['name'] ?></h3>
          
          
          <span flex class="flex"></span>
          
          <md-input-container>
              <md-select ng-change="updateAssClient(this)" ng-model="assignedClient" aria-label="Select client" ng-model-options="{trackBy: '$value.id'}">
                  <md-option ng-repeat="client in clients" ng-value="client">{{client.name+'('+client.company+')'}}</md-option>
            </md-select>
              <md-tooltip>Client</md-tooltip>
          </md-input-container>
          
          
          <h3 ><?php echo $user['first_name'].' '.$user['last_name'] ?>
              <md-tooltip>
                  Last login: <?php echo date('D, d-m-Y H:i:s',$user['last_login']);?>
              </md-tooltip>
          </h3>
          
          <md-button class="md-icon-button md-button md-ink-ripple" ng-click="changePass()" type="button" aria-label="Change password" ng-class="{ active: demoCtrl.$showSource }" >
              <md-icon  md-colors="{fill:'accent'}" md-svg-src="key-change" class="ng-scope ng-isolate-scope" aria-hidden="true">
              </md-icon>
              <md-tooltip>
                  Change Password
              </md-tooltip>
          </md-button>
          <md-button class="md-accent md-icon-button md-button md-ink-ripple" ng-click="logout()" type="button" aria-label="Logout" aria-hidden="false">
              <md-icon md-colors="{fill:'accent'}" md-svg-src="logout" class="ng-scope ng-isolate-scope" aria-hidden="true">
              </md-icon>
              <md-tooltip>
                  Logout
              </md-tooltip>
          </md-button>
    </div>
</md-toolbar>


<md-content layout="row" flex="100" >
<!-- Container #3 -->
    <md-sidenav md-component-id="left" md-is-locked-open="$mdMedia('gt-sm')" class="md-sidenav-left md-whiteframe-z2" layout="row" layout-wrap flex-xs="35" flex="25">
        <header layout-padding id="hdr" flex layout="row">
            <a ng-href="<?php echo site_url($appdir)?>" flex="100" layout="row" layout-wrap layout-align="center center">
            <img src="<?php echo $assetdir?>/img/logo.png" alt="<?php echo site_url()?>" class="side-logo" >
            <h3 flex="100"> Project's AVR Portal</h3>
            <md-tooltip>
                  Home
            </md-tooltip>
            </a>
        </header>
           
      <md-list flex="100" layout="row" layout-wrap>
          <md-list-item class="menu-list md-1-line" ng-repeat="menu in menus" layout="row" flex="100" layout-wrap ng-href="{{ !ismenuselected(menu.link)? '<?php echo site_url()?>/'+menu.link : ''}}" >
              <md-icon md-svg-icon="{{menu.icon}}" class="md-avatar-icon" md-colors="{background:ismenuselected('{{menu.link}}')?'accent':'grey-600' }"></md-icon>
            <p layout="row" classs="md-caption" flex>{{menu.name}} <div ng-if="reqCounts[menu.name]" class="md-button md-icon-button" md-colors="{background:'accent-50'}" style="min-width:40px">{{reqCounts[menu.name]}}</div></p>
            <md-tooltip>{{menu.name}}</md-tooltip>
              <!--<md-divider md-colors="{'border-color':ismenuselected('{{menu.link}}')?'accent':'grey-600' }"></md-divider></md-divider>-->
        </md-list-item>
      </md-list>
    </md-sidenav>
        

<md-content flex layout = "column" layout-padding>
<md-progress-linear ng-show="loading" md-mode="query"></md-progress-linear>
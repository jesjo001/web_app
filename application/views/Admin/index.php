<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

?>

<title>Dashboard</title>

<div flex data-ng-init="dashboard_page()" layout="row" layout-wrap>

    <md-card flex="100" flex-gt-sm="45" ng-repeat="chart in charts" layout-padding layout="column" layout-align="center center" style="">
        <p class="md-title">{{chart.title}}</p>

        <canvas chart-type id="{{chart.chartid}}" class="chart __chart-type" chart-data="chart.data"
                chart-labels="chart.labels" chart-series="chart.series" chart-options="chart.options"
                chart-dataset-override="chart.datasetOverride" chart-click="chart.onClick">
        </canvas

    </md-card>

</div>
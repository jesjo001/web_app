<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee *
 */


error_reporting(E_ALL);
ini_set('display_errors',1);

class Basic{
    
    protected static $ci;
    
    public function __construct(){
        self::$ci =& get_instance();
    }


    public function find_by_id($id){
        return static::find_by_key($id);
    }


    public function get_instance($array=[]){
        return static::setup($array);
    }


    /**
     * Generate batch code for newly created requests
     * @return string The batch code for the requests
     */
    public function generate_batch(){
        return date('ymdHi');
    }

    /**
     * Fetch class by key value
     * @param int $key key value of class' row to be returned
     * @return class object Return object if found, false otherwise.
     */
    public function find_by_key($key=0){
        if(!is_array($key))
        return ($one = static::find_by_sql("SELECT * from ".static::$table_name." where ".static::$key." = $key"))?array_shift($one):FALSE;
        else
        return ($all = static::find_by_sql("SELECT * from ".static::$table_name." where ".static::$key." in (".join(',',$key).")"))?$all:FALSE;
    }

    public function fetch_by_var($select_array,$var_array,$options=''){
        if(!is_array($var_array))
            return false;


        $select = is_array($select_array)?implode(',',$select_array):$select_array;

        $var_search = 'select '.$select.' from '.static::$table_name.' where ';

        $keys = array_keys($var_array);
        $n = 0;

        foreach($var_array as $var){


            if($n>0)
                $var_search .= " and ";
            $k = $keys[$n];
            $v = $var;
            if(is_array($v)){
                if(count($v) == 2) {
                    if(!strstr($k,'date'))
                    $var_search .= $k . ' between "' . $v[0] . '" and "' . $v[1] . '"';
                    else
                        $var_search .=  'cast('.$k .' as datetime) between cast("'. $v[0] . '" as datetime) and cast("' . $v[1] . '" as datetime)';
                }
                else{
                    $var_search .= $k.' in ("'.implode('","',$v).'")';
                }
            }
            else {
                if(!is_int($k))
                $var_search .=  $k.'="'.$v.'"';
                else{
                    $var_search .= $var;
                }
            }
            $n++;
        }
        $var_search .=" ".$options;

        return static::find_by_sql($var_search);

    }
/**
 * Fetch all rows from class
 * @param type $order Oreder by class' key
 * @return array Array of class objects
 */
    public static function find_all($order = 'DESC'){
        $order =(stristr($order,'DESC'))?'DESC' : 'ASC';
        return static::find_by_sql("SELECT * from ".static::$table_name." order by ".static::$key." $order ");
     }


    /**
     * Run a query and return the status of the quey
     * @param type $sql SQL to be run
     * @return boolean Status of query
     */
    public static function query_by_sql($sql){
        return self::$ci->db->query($sql);
    }

    
    /**
     * Return matching rows by sql
     * @param type $sql SQL to be fetched
     * @return array array of matching rows, false if no match found
     */
    public static function find_by_sql($sql){
        $result = self::$ci->db->query($sql);
        $find = [];
        foreach($result->result() as $row){
            $find[] = static::setup($row);
        }
        if(is_array($find))return (count($find) > 0)? $find : false;
        else return false;
    }
    
/**
 * Instantiate rows fetched from database to class object
 * @param type $result DB result
 * @return \static object of class
 */
    protected static function setup($result){
        $class = new static();
        $instance = new static();
        
        foreach(array_keys((array)  get_object_vars($class)) as $i=>$var){
        if(property_exists($instance,$var) && isset($result->$var))$instance->$var = $result->$var;
        }
        
        return $instance;
    }
    
    
    /**
     * Update row in DB
     * @return boolean true on successful, false otherwise
     */
    public function update(){
    		if(!$this)return false;
    		if(!static::$table_columns)return false;
    		
    		foreach(static::$table_columns as $col){
    		if(isset($this->$col))$data[$col] = $this->$col;
    		}
                //load CI and use db class
                $ci = self::$ci;
    		$ci->db->where(static::$key,$this->{static::$key});
    		$ci->db->update(static::$table_name,$data);


        //return $ci->db->affected_rows() > 0;
        return $ci->db->trans_status();
    }
    
    /**
     * Create new row from class object 
     * @return boolean true on successful, false otherwise
     */
    public function create(){
    		if(!$this)return false;
    		if(!static::$table_columns)return false;
    		
    		
    		foreach(static::$table_columns as $col){
    		$data[$col] = $this->$col;
    		}

            if(isset(static::$key))unset($data[static::$key]);
    		
                //load CI and use db class
                $ci = self::$ci;
    		$ci->db->insert(static::$table_name,$data);

    		return ($ci->db->affected_rows())?$ci->db->insert_id():false;
    }
    
    
    /**
     * Delete class object from DB
     * @return boolean true on successful, false otherwise
     */
    public function delete(){
    		if(!$this->{static::$key})return false;
    		
                //load CI and use db class
                $ci = self::$ci;
    		self::$ci->db->delete(static::$table_name,array(static::$key=>$this->{static::$key}));
    		return (bool)self::$ci->db->affected_rows();
    		}
    		
    
    
    
}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */
require_once 'Basic.php';

class Reports extends Basic{

    public $id;
    
    public $request_id;
    
    public $action;

    public $summary;

    public $description;

    public $address_exists;
    
    public $resides;
    
    public $customer;

    public $owner;

    public $area;

    public $completion;

    public $finish;

    public $color;

    public $structure_0;

    public $structure_1;

    public $structure_2;

    public $interviewed;

    public $interviewee;

    public $gps;


    public $dispatch_id;
    
    public $admin_id;
    
    public $date_created;
    
    public $date_updated;

    public $date_approved;
    
    public static $key = 'id';
    
    protected static $table_name = 'reports';

    protected static $table_interviewee_name = 'reports_interviewees';

    protected static $table_columns = array('request_id','action','summary','description','address_exists','resides','customer','owner','area','completion','finish','color','structure_0','structure_1','structure_2','interviewed','gps','dispatch_id','admin_id','date_created','date_approved','date_updated');
    
    protected static $not_update = array('dispatch_id','date_created');

    protected static $interviewee_columns = array('report_id','name','phone','gender','address','relationship');

    protected static $interviewee_not_update = array('id','report_id');

    
    
    public function __construct(){
    		parent::__construct();
    }
    
    
    public static function fetch_report_preview($report_id){
            $sql = "
            SELECT id,request_id, (case
            when address_exists = 'yes' and resides = 'yes' then 'positive'
            else 'negative' end) as status,
            summary, dispatch_id, admin_id  from ".self::$table_name." where id = ".intval($report_id);

            return self::find_by_sql($sql);
    }

    public function find_by_id($id){
        return self::find_by_key($id);
    }

    public static function get_action($id=0){
            $sql = "
            SELECT action from ".self::$table_name." where id = ".intval($id);

            return ($rep = self::find_by_sql($sql))?array_shift($rep)->action:'';
    }
    
    public static function rejectReport($id=0){
        if(intval($id)<1)return false;
        $ci = parent::$ci;

        $report = new self;
        $report->id = $id;
        $report->action = self::get_action($id).'Report rejected;';
        $report->admin_id = $ci->session->user_id;
        $report->date_updated = date('Y-m-d H:i:s');

        return $report->update();
    }

    public static function get_report_type($exists,$resides){
        return strtolower($exists) == 'yes' && strtolower($resides) == 'yes'?'positive':'negative';
    }

    public static function get_state($state){
		$status=false;
		switch($state){
				case 'submitted':
						$status = 1;
						break;
				case 'approved':
						$status = 2;
						break;
				case 'rejected':
						$status = 3;
						break;
				default:
						$status = false;
						break;
						}
			return $status;
			}
		

    public function create(){
        $db = & parent::$ci->db;
        $db->trans_start();
        $report = parent::create();
        if($report){
            if($this->interviewee) {
                $this->interviewee['report_id'] = $report;
                if ($this->add_report_interviewee($this->interviewee)) {
                    $db->trans_complete();
                    return $report;
                }
            }
            else{
                $db->trans_complete();
                return $report;
            }
        }
        return false;
    }

    public function update(){
        $db = & parent::$ci->db;
        $db->trans_start();
        $report = parent::update();
        if($report){
            if($this->interviewee) {
                if(!isset($this->interviewee["id"])){
                    $this->interviewee['report_id'] = $this->id;
                    if($this->add_report_interviewee($this->interviewee)){
                    $db->trans_complete();
                    return true;
                    }
                }
                else {
                    if ($this->update_report_interviewee($this->interviewee)) {
                        $db->trans_complete();
                        return true;
                    }
                }
            }
            else{
                $db->trans_complete();
                return true;
            }
        }
        return false;
    }

    public function add_report_interviewee($interviewee)
    {
        if (intval($interviewee['report_id']) != $interviewee['report_id'] || $interviewee['report_id'] == '') return false;

        $new_interviewee = [];
        foreach ($interviewee as $k => $v) {
            if (in_array($k, self::$interviewee_columns) && $k != 'id')
                $new_interviewee[$k] = $v;
        }
        $db = & parent::$ci->db;
        $db->insert(self::$table_interviewee_name, $new_interviewee);
        return $db->affected_rows()>0;
    }


    public function update_report_interviewee($interviewee)
    {
        if (intval($interviewee['id']) != $interviewee['id'] || $interviewee['id'] == '') return false;

        $new_interviewee = [];
        foreach ($interviewee as $k => $v) {
            if (in_array($k, self::$interviewee_columns) && !in_array($k, self::$interviewee_not_update))
                $new_interviewee[$k] = $v;
        }
        $db = & parent::$ci->db;
        $db->set($new_interviewee);
        $db->where('id', $interviewee['id']);
        $db->update(self::$table_interviewee_name);
        return $db->trans_status();
    }

    public static function fetch_report_interviewee($report_id)
    {
        if (intval($report_id) != $report_id || $report_id == '') return false;
        $db = & parent::$ci->db;

        $db->select('*')->where('report_id',$report_id);
        $result = $db->get(self::$table_interviewee_name);
        $intv = [];
        foreach($result->result() as $row){
            $intv[] = $row;
        }
        return array_shift($intv);
    }

    public static function new_interviewee()
    {
        $db = & parent::$ci->db;

        //#TODO dynamically retrieve data base name
        $db->select('COLUMN_NAME')->where('TABLE_SCHEMA','avr_db');
        $db->where('TABLE_NAME',self::$table_interviewee_name);
        $result = $db->get('INFORMATION_SCHEMA.COLUMNS');
        $intv = [];

        foreach($result->result() as $row){
            $intv[$row->COLUMN_NAME] = null;
        }
        
        return (object)$intv;
    }

    public Static function setup($result){
        $instance = parent::setup($result);
        if($instance->{self::$key} && $instance->{self::$key} != ''){
            $interviwees = self::fetch_report_interviewee($instance->{self::$key});
            if(!empty($interviwees) && is_object($interviwees))
                $instance->interviewee = $interviwees;
        }
        return $instance;
    }
		
		
		


}
<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee *
 */


require_once 'Companies.php';


class Users extends Basic{
    
    public $id;
    
    public $username;
    
    public $first_name;
    
    public $last_name;
    
    public $email;
    
    public $company;

    public $remember_code;

    public $status;
    
    public $last_login;
    
    public static $key = 'id';
    
    protected static $table_name;
    
    protected static $user_branch_table_name = 'users_branch';

    protected static $branch_table_name = 'branches';

    protected static $company_table_name = 'companies';

    protected $table_colums = array('id','username','first_name','last_name','email','remember_code','status','last_login');
    






    public function __construct(){
        parent::__construct();
        
        // load ci instance
        $ci = parent::$ci;
        
        //set $table_name
        $ci->load->library('ion_auth');
        $ci->load->config('ion_auth', TRUE);
        self::$table_name = parent::$ci->config->item('tables','ion_auth')['users'];
    }
    
    
    //public function 
    
    
    public function get_join(){
        //Session variable for user.id
        return 'user_id';
    }

    public static function get_user_companyid($user_id){
        $db = parent::$ci->db;
        $sql = "SELECT b.company_id id from ".self::$branch_table_name." b inner join ".self::$user_branch_table_name." ub where ub.user_id = $user_id";
        return ($user = self::find_by_sql($sql))?array_shift($user)->id : false;
    }

    public static function get_user_branchid($user_id){
        $db = parent::$ci->db;
        $sql = "SELECT branch_id id from ".self::$user_branch_table_name." ub where ub.user_id = $user_id";
        return ($user = self::find_by_sql($sql))?array_shift($user)->id : false;
    }

    
    public static function fetch_logon_details($user_id){
        //Get values for session userdata on sucessful login
        return self::find_by_id($user_id);
    }
    
    
    public function fetch_all_dispatchers(){
	    $ci = & parent::$ci;
        $ci->load->config('appconfig',true);
	    $join = $ci->config->item('join', 'ion_auth');
	    $tables = $ci->config->item('tables', 'ion_auth');
	    $groups = $ci->config->item('groups', 'appconfig');
        $d_id = $groups['dispatchs_group'];
    		
    		$sql = "Select u.* from ".self::$table_name." u inner join {$tables['users_groups']} ug on u.".self::$key." = ug.{$join['users']}
    		  where ug.{$join['groups']} = $d_id ";
    		
    		return self::find_by_sql($sql);   
    
    }


    public function fetch_dispatchers_by_branch($branch_id){
	    $ci = & parent::$ci;
        $ci->load->config('appconfig',true);
	    $join = $ci->config->item('join', 'ion_auth');
	    $tables = $ci->config->item('tables', 'ion_auth');
	    $groups = $ci->config->item('groups', 'appconfig');
        $d_id = $groups['dispatchs_group'];

    		$sql = "Select u.* from ".self::$table_name." u inner join {$tables['users_groups']} ug on u.".self::$key." = ug.{$join['users']}
    		 inner join ".self::$user_branch_table_name." ub on u.".self::$key." = ub.user_id  where ug.{$join['groups']} = $d_id and ub.branch_id = $branch_id";

    		return self::find_by_sql($sql);

    }
    
    
    public function fetch_all_admins($branch_id=0){
	    $ci = & parent::$ci;
	    $join = $ci->config->item('join', 'ion_auth');
	    $tables = $ci->config->item('tables', 'ion_auth');
	    $a_id = $ci->config->item('admins_group', 'ion_auth');
    		
    		$sql = "Select u.* from ".self::$table_name." u inner join {$tables['users_groups']} ug on u.".self::$key." = ug.{$join['users']} where ug.{$join['groups']} = $a_id";
    		return self::find_by_sql($sql);
    
    }

    public function fetch_admins_name_id_by_branch($branch_id=0){
	    $ci = & parent::$ci;
        $ci->load->config('appconfig',true);
        $join = $ci->config->item('join', 'ion_auth');
        $tables = $ci->config->item('tables', 'ion_auth');
        $groups = $ci->config->item('groups', 'appconfig');
	    $a_id = $groups['admins_group'];

        $sql = "Select u.* from ".self::$table_name." u inner join {$tables['users_groups']} ug on u.".self::$key." = ug.{$join['users']}
    		 inner join ".self::$user_branch_table_name." ub on u.".self::$key." = ub.user_id  where ug.{$join['groups']} = $a_id and ub.branch_id = $branch_id";

        return self::find_by_sql($sql);

    }

    public function fetch_admins_name_id_by_company($comp_id=0){
	    $ci = & parent::$ci;
        $ci->load->config('appconfig',true);
        $join = $ci->config->item('join', 'ion_auth');
        $tables = $ci->config->item('tables', 'ion_auth');
        $groups = $ci->config->item('groups', 'appconfig');
	    $a_id = $groups['admins_group'];

        $sql = "Select u.* from ".self::$table_name." u inner join {$tables['users_groups']} ug on u.".self::$key." = ug.{$join['users']}
    		    inner join ".self::$user_branch_table_name." ub on u.".self::$key." = ub.user_id
                inner join ".self::$branch_table_name." b on b.id = ub.branch_id
                inner join ".self::$company_table_name." c on b.company_id = c.id 
                where ug.{$join['groups']} = $a_id ";

        return self::find_by_sql($sql);

    }




    public Static function setup($result){
        $instance = parent::setup($result);
        if($instance->{self::$key} && $instance->{self::$key} > 0){
            $company = Companies::get_company_by_user($instance->{self::$key});
            if(!empty($company))
                $instance->company = $company;
        }
        return $instance;
    }
}
<?php
/**
 * Copyright (c)
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony
 * Email: Adibas03@gmail.com
 * Date: 10/2016
 * Licensed to: LicensedTo
 * License subject to changes based on agreement between  Author and Licensee
 */



require_once 'Basic.php';


class Companies extends Basic
{

    public $id;

    public $name;

    public $logo;

    public $address;

    public $email;

    public $date_created;

    public $date_updated;

    public $created_by;

    public $updated_by;

    public static $key = 'id';

    protected static $table_name = 'companies';

    protected static $user_branch_table_name = 'users_branch';

    protected static $company_contact_table_name = 'company_contacts';

    protected static $company_tat_table_name = 'company_tat';

    protected static $branch_table_name = 'branches';

    protected static $table_columns = array('id', 'name', 'address', 'email', 'date_created', 'date_updated', 'created_by', 'updated_by');

    protected static $not_update = array('id', 'name', 'date_created', 'created_by');


    public function __construct()
    {
        parent::__construct();

        // load ci instance
        $ci = parent::$ci;
    }


    public static function get_company_by_user($user_id)
    {
        $db = parent::$ci->db;
        $sql = "SELECT c.* from " . self::$table_name." c 
        inner join ".self::$branch_table_name." b on c.id = b.company_id 
        inner join ".self::$user_branch_table_name." ub on b.id = ub.branch_id  where user_id = $user_id";
        return ($company = self::find_by_sql($sql)) ? array_shift($company) : false;
    }

    public static function get_company_by_branch($branch_id)
    {
        $db = parent::$ci->db;
        $sql = "SELECT c.* from " . self::$table_name." c 
        inner join ".self::$branch_table_name." b on c.id = b.company_id 
        where b.id = $branch_id";
        return ($company = self::find_by_sql($sql)) ? array_shift($company) : false;
    }

    

}
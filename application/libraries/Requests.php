<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

require_once 'Basic.php';

class Requests extends Basic{

    public $id;

    public $client_branch_id;

    public $branch_id;

    public $batch_id;

	public $customer_id;
    
    public $name;
    
    public $address;
    
    public $landmark;
    
    public $telephone;
    
    public $status;

    public $request_date;
    
    public $date_created;

    public $created_by;
    
    public $report_id;

    public $report_status;
    
    
    
    
    public static $key = 'id';
    
    protected static $table_name = 'requests';
	
		protected static $phone_table_name = 'requests_phones';
	
	    protected static $report_table_name = 'reports';
	    
	    protected static $dispatch_table_name = 'requests_dispatch';
	    
	    protected static $table_columns = array('client_branch_id','branch_id','batch_id','customer_id','status','name','address','landmark','report_status','request_date','date_created','created_by','report_id','report_status');
	
	    
	    
	    
	    public function __construct(){
	    		parent::__construct();
	    }


		//--Dispatch Functions

			public static function fetch_dispatch_pending_jobs($id=0){
				$sql = "Select r.id,client_branch_id,name,address,landmark,request_date,dispatch_id from requests r inner join requests_dispatch rd on r.id = rd.request_id and rd.dispatch_id = $id and status<".self::get_state('reported');
				//return self::fetch_group_members_info('pending',$select = array('client_branch_id','name','address','landmark','request_date'),$conditions = array('dispatch_id'=>$id));
				return self::find_by_sql($sql);
			}

	    //--Admin & Client Functions
			public static function count_pending_by_branch($branch){
	           $count = self::find_by_sql("SELECT count(".self::$key.") id from ".self::$table_name." where client_branch_id = ".intval($branch)." and status < ".self::get_state('approved'));
				return array_shift($count)->id;
			}

			public static function count_pending_review_by_branch($branch){
				$count = self::find_by_sql("SELECT count(".self::$key.") id from ".self::$table_name." where client_branch_id = ".intval($branch)." and status = ".self::get_state('reported'));
				return array_shift($count)->id;
			}

			public static function count_approved_by_branch($branch){
				$count = self::find_by_sql("SELECT count(".self::$key.") id from ".self::$table_name." where client_branch_id = ".intval($branch)." and status = ".self::get_state('approved'));
				return array_shift($count)->id;
			}
	  
	    
			public static function find_pending_by_branch($branch){
	           return self::find_by_sql("SELECT * from ".self::$table_name." where client_branch_id = ".intval($branch)." and status < ".self::get_state('approved'));
			}

	                
	     public static function find_pending_review_by_branch($branch){
					return self::find_by_sql("SELECT * from ".self::$table_name." where client_branch_id = ".intval($branch)." and status = ".self::get_state('reported'));
			}
			
			
			public static function find_approved_by_branch($branch){
	           return self::find_by_sql("SELECT * from ".self::$table_name." where client_branch_id = ".intval($branch)." and status = ".self::get_state('approved'));
			}

			public static function fetch_request_dispatch_info($job_id){
				$req = self::find_by_sql("SELECT id,dispatch_id name,date request_date,updated_by created_by from ".self::$dispatch_table_name." where request_id = $job_id");
				if($req){
					$req = array_shift($req);
					$a = new stdClass();//(object)array();
					$a->id = $req->id;
					$a->dispatch_id = $req-> name;
					$a->time = $req->request_date;
					$a->updated_by = $req->created_by;
					return $a;
				}
				else return null;

			}
			
			
			public static function fetch_request_dispatch($job_id){
    				return ($req = self::find_by_sql("SELECT dispatch_id name from ".self::$dispatch_table_name." where request_id = $job_id"))?array_shift($req)->name:null;
    				
    			}
    			
    			
    			public static function fetch_group_members_info($state = 'all',$select = array(),$conditions = array(),$limit=0,$offset=''){
    			
	    			if($state == 'pending') $status = "status < ".self::get_state('approved');  
    				else if($state != 'all')$status = " status = ".self::get_state($state);
    				else if($state == 'all')$status = '';
    				
    				$db = & parent::$ci->db;
    				
    				if(!empty($conditions)){
    				$i =0;$where = '';
    				foreach($conditions as $k=>$v){
    				if($i > 0 and $i < count($conditions))$where .= ' and ';
    				$where .= " $k = '$v'";  
    				$i++;
    				}
    				}
    				
    				if(empty($select))$select[] = self::$key;
    				
    				$sql = "select ".implode(',',$select)." from ".self::$table_name." ";
    				if(isset($where))$sql .= " where $where";
    				if(isset($where) && $status != '') $sql .=' and ';
    				$sql .= $status;
    				if($limit > 0)$sql .= " limit $limit ";
    				if($offset != '') $sql .= " offset $offset ";
    				return self::find_by_sql($sql);
    			}


			public static function removeReport($id=0){
				if(intval($id)<1) return false;
				$temp = new self;
				$temp->id = $id;
				$temp->report_id = 0;
				$temp->report_status = '';
				$temp->status = self::get_state('assigned');

				return $temp->update();
			}
	
    			
    		public static function update_dispatch($request,$dispatch){
    				$ci=& parent::$ci;
    				$ci->load->library('users');
    				$join = $ci->users->get_join();
    				$db = $ci->db;
    				$sql = "INSERT into ".self::$dispatch_table_name." (request_id,dispatch_id,updated_by,date) VALUES ('$request','$dispatch','{$ci->session->userdata($join)}','".date('Y-m-d H:i:s')."') on duplicate key update dispatch_id = values(dispatch_id),updated_by = values(updated_by),date = values(date)";
    				$db->query($sql);
    				return ($ar = $db->affected_rows() > 0)?$ar:false;
    			}

		public static function request_exists($request){
			$sql = "select count(id) batch_id from ".self::$table_name." where name = '".$request->name."' and landmark = '".$request->landmark."' and address = '".$request->address."'";
			return ($res = self::find_by_sql($sql))?arrayshift($res)->batch_id > 0:false;
		}

		public static function is_reported($id=0){
			if($id<1) return false;
			$newreq = new self;
			$newreq = $newreq->find_by_id($id);
			return !($newreq->status<self::get_state('reported'));
		}
			
		
		protected function set_status($state){
			if(!$this)return false;
				$status = self::get_state($state);
				if(is_int($status)){
				$sql = "update ".self::$table_name." set status = $status where ".self::$key." = ".$this->{self::$key};
		$db = &parent::$ci->db;
		if($db->query($sql))return $status;
		else return false;
		}
		else return false;
		} 
		
		
		public static function get_state($state){
		$status=false;
		switch($state){
				case 'new':
						$status = 0;
						break;
				case 'viewed':
						$status = 1;
						break;
				case 'assigned':
						$status = 2;
						break;
				case 'reported':
						$status = 3;
						break;
				case 'approved':
						$status = 4;
						break;		
				case 'returned':
						$status = 5;
						break;
				default:
						$status = false;
						break;
						}
			return $status;
			}
			
			
		public static function get_phones($id){
		$result = parent::$ci->db->get_where(self::$phone_table_name,array('request_id'=>$id));
		$find = array();
		foreach($result->result() as $row){
            $find[] = $row->telephone;
        }
        return $find;
     }
  
		
		
		public function save_phones(){
		if($this->{self::$key}){
		if($this->telephone == '')return;
		$phones = explode(',',$this->telephone);
		$db = &parent::$ci->db;
		foreach($phones as $one){
		$data = [];
		$data['request_id'] = $this->id;
		$data['telephone'] = $one;
		$db->insert(self::$phone_table_name,$data);
		}
		}
		}
		
		
		public Static function setup($result){
		$instance = parent::setup($result);
		if($instance->{self::$key} && $instance->{self::$key} != ''){
		$phones = self::get_phones($instance->{self::$key});
		if(!empty($phones))
		$instance->telephone = implode(',',$phones);
		if(!isset($instance->dispatch_id) || $instance->dispatch_id == '')$instance->dispatch_id = self:: fetch_request_dispatch($instance->{self::$key});
		}
		return $instance;
		}
		
		
		public function create(){
		
		if(!$this)return false;
    	if(!self::$table_columns)return false;
		$this->status = 0;
		$this->report_id = 0;
		if(!$this->branch_id)$this->branch_id = '';
		if(!$this->customer_id)$this->customer_id = '';
		$this->report_status = '';
		$this->date_created = date('Y-m-d H:i:s');
		$this->created_by = parent::$ci->session->userdata('user_id');

		$db = & parent::$ci->db;

    	if($id = parent::create()){
    	$this->{self::$key} = $id;
    	$this->save_phones();
    	$this->set_status('new');
    	return $id;}
    	else return false;
    		
		}
		
		
		public function find_by_id($id){
			return self::find_by_key($id);
		}

}
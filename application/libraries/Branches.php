<?php
/**
 * Copyright (c)
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony
 * Email: Adibas03@gmail.com
 * Date: 10/2016
 * Licensed to: LicensedTo
 * License subject to changes based on agreement between  Author and Licensee
 */



require_once 'Companies.php';


class Branches extends Basic
{

    public $id;

    public $company_id;

    public $name;

    public $location;

    public $date_created;

    public $date_updated;

    public $created_by;

    public $updated_by;

    public static $key = 'id';

    protected static $table_name = 'branches';

    protected static $user_branch_table_name = 'users_branch';

    protected static $company_contact_table_name = 'company_contacts';

    protected static $company_table_name = 'companies';

    protected static $table_columns = array('id', 'name', 'location', 'date_created', 'date_updated', 'created_by', 'updated_by');

    protected static $not_update = array('id', 'name', 'company_id', 'date_created', 'created_by');


    public function __construct()
    {
        parent::__construct();

        // load ci instance
        $ci = parent::$ci;
    }


    public static function get_branch_by_user($user_id=0)
    {
        $sql = "SELECT b.* from " . self::$table_name." b 
        inner join ".self::$user_branch_table_name." ub on b.id = ub.branch_id  where user_id = $user_id";
        return ($branch = self::find_by_sql($sql)) ? array_shift($branch) : false;
    }

    public static function get_company_branch($branch_id=0,$company_id=0){
        $sql = "SELECT * FROM ".self::$table_name." where company_id = $company_id and id = $branch_id";
        return ($branch = self::find_by_sql($sql)) ? array_shift($branch) : false;
    }

    public static function fetch_all_company_branches($company_id = 0)
    {
        $sql = "SELECT * FROM ".self::$table_name." Where company_id = $company_id";
        return self::find_by_sql($sql);
    }

    public static function get_company_branch_by_user($user_id=0,$branch_id=0){
        $company = Companies::get_company_by_user($user_id)->id;
        return self::get_company_branch($branch_id,$company);
    }

    

}
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */

 error_reporting(E_ALL);
 ini_set('display_errors',1);
//ini_set('xdebug.max_nesting_level', 256);

    date_default_timezone_set("Africa/Lagos");

class MY_Controller extends CI_Controller {

    //protected $data = Array(); //protected variables goes here its declaration
    protected static  $user_app;

    function __construct()
    {

        parent::__construct();

        $this->load->config('appconfig', true);
        $app_header = $this->config->item('app_header', 'appconfig');
        $app_access = $this->config->item('app_access', 'appconfig');
        static::$data['licensed'] = $this->config->item('company','appconfig')['name'];
        static::$data['mapkey'] = $this->config->item('api_key','appconfig')['maps'];

        //Check if message from app
        if (isset($_REQUEST['user-type'])) $user_header = $_REQUEST['user-type'];
        if (isset($user_header) && $user_header == $app_header)
            self::$user_app = 'mobile';

        if (self::$user_app == 'mobile') {
            //load helpers and everything here like form_helper etc
            header('Access-Control-Allow-Origin: http://localhost:8000');
            header('Access-Control-Allow-Headers: content-type');
            header('Access-Control-Allow-Credentials: true');
        }
        $this->load->model('login_model');

        //check and prepare for redirection 

        $url = current_url();
        $base_url = site_url();
        $redirect = ($this->input->get('redirect')) ? 1 : 0;
        if ($redirect) {
            $redirect_string = $this->input->get('redirect');
            if (stristr($redirect_string, 'index.php')) $redirect_string = substr($redirect_string, stripos($redirect_string, 'index.php') + 9);
            if (stristr($redirect_string, $base_url)) $redirect_string = substr($redirect_string, stripos($redirect_string, $base_url) + str_len($base_url) + 1);
            $redirect = ($redirect > 0 && $redirect != '') ? $redirect_string : 0;
        }



        //Redirect as required
        if ($this->ion_auth->logged_in()) {

            //Check if request from mobille and if user group exists on mobile
            if (self::$user_app == 'mobile'){
            $user_groups = $this->ion_auth->get_users_groups()->result()[0];
            if(!in_array($user_groups->name,$app_access))die('noaccess');
            }

            //Set users company details
            $this->load->library('companies');

            $user_id = $this->session->userdata('user_id');
            $company = $this->companies->get_company_by_user($user_id);
            static::$data['company'] =array('name'=>$company->name);

            if (self::$user_app != 'mobile')
                if($redirect ) redirect($redirect);
        } else {

            if (!stristr($url, 'login')) {
                if (self::$user_app == 'mobile') die('noaccess');
                elseif ($redirect) redirect('login?redirect=' . $redirect);//redirect('login');
                else (($nurl = preg_replace("#$base_url#", '', $url)) != '') ? redirect('login?redirect=' . $nurl) : redirect('login');
            }
            // else if(!stristr($url,'logon') && self::$user_app == 'mobile')die('noaccess');
            //echo var_dump($this->session->all_userdata());
        }
    }
    
             
    public function account_exists($uname){
        return $this->login_model->account_exists($uname);
    }
    
    public function get_location(){
               if($this->ion_auth->in_group(1))return('ok');
               else if($this->ion_auth->in_group(2))return('ok');
               else if($this->ion_auth->in_group(3))return('admin');
               else if($this->ion_auth->in_group(4))return('topsupervisor');
               else if($this->ion_auth->in_group(5))return('clientuser');
               else if($this->ion_auth->in_group(6))return('client');
               else if($this->ion_auth->in_group(10))return('superuser');
		    
    }
    
    public function get_logged_user_details(){
        return $this->ion_auth->user()->row();
    }

    protected function get_user_from_session(){
        $session_vars = array('username','first_name','last_name','last_login','email');
        foreach($session_vars as $one){
            $data[$one] = $this->session->userdata($one);
        }
        return $data;
    }

    protected function send_json_response($response){
        die(json_encode($response));
    }
    
    protected function prepare_export($ids=[]){
        if(!is_array($ids) || count($ids)<1)
            return false;
        $this->load->library('requests');
        $this->load->library('reports');
        $this->load->library('companies');
        $this->load->library('branches');

        $disallowed_requests = ['id','client_branch_id','batch_id','status','date_created','created_by','dispatch_id','report_id','report_status'];
        $disallowed_reports = ['id','request_id','action','status','dispatch_id','admin_id','date_created','date_updated'];
        $disallowed_interviewee = ['id','report_id'];

        if($ids){
            $found = $this->requests->find_by_id($ids);
            if(is_array($found) && count($found)>0)
                foreach($found as $one){
                    $one->branch = $this->branches->find_by_id($one->client_branch_id)->name;
                    $one->company = $this->companies->get_company_by_branch($one->client_branch_id)->name;
                    $one->report = $this->reports->find_by_id($one->report_id);

                    foreach($disallowed_requests as $k) {
                        if (property_exists($one,$k))
                            unset($one->$k);
                    }

                    if(!$one->report)$one->report = new $this->reports();

                    foreach($disallowed_reports as $k) {
                        if (property_exists($one->report,$k))
                            unset($one->report->$k);
                    }

                    if(!$one->report->interviewee)$one->report->interviewee = $this->reports->new_interviewee();

                    foreach($disallowed_interviewee as $k) {
                        if (property_exists($one->report->interviewee,$k))
                            unset($one->report->interviewee->$k);
                    }
                }
            return $found;
        }
        else return false;
        
    }



    public function export_request(){
        
        $requests = $this->input->post();
        $requests = isset($requests['requests']) && is_array($requests['requests'])?$requests['requests']:false;
        
        $to_export = $this->prepare_export($requests);
        if($to_export)
            die(json_encode($to_export));
        else
            return false;

    }

    public function print_pdf_report($id=0){
        if($id == 0)
            redirect('login');
        $this->load->library('m_pdf');
        $this->load->library('companies');

        $this->load->config('appconfig',true);
        $base = $this->config->item('pdf_template','appconfig');
        $pre = '<body style="width:960px;margin:0 auto;font-size:24px;background-color:#fff;">';
        $post = '</body>';

        $user = $this->uri->segment(1);
        if($user == 'admin')
            $client = $this->session->userdata('client')['id'];
        if($user == 'client')
            $client = $this->session->userdata('branch')->id;
        if($client){
            $client = $this->companies->find_by_id($client);
            $client = $client->logo;
        }
        if($client)
            $base = str_replace('__clilogosrc',base_url('assets/uploads/logo').'/'. $client,$base);

        $comp = $this->config->item('company','appconfig');
        $base = str_replace('__licensed',strtoupper($comp['name']),$base);
        $base = str_replace('__compaddress',$comp['address'],$base);
        $base = str_replace('__logosrc',base_url('assets/img').'/'. $comp['logo'],$base);
        $base = str_replace('__signsrc',base_url('assets/img/sign.png'),$base);

        //clilogosrc

        $prep = array($id);

        $export = $this->prepare_export($prep);
        $export = array_shift($export);

        $rep = $export->report;
        unset($export->report);

        $intv = $rep->interviewee;
        unset($rep->interviewee);

        $nintv = (object)[];
        foreach($intv as $k=>$v){
            $nk = 'interviewee_'.$k;
            $nintv->$nk = $v;
        }

        $fin = (object) array_merge((array)$rep,(array)$export,(array)$nintv);

        $base = str_replace('__page_break','',$base);

        foreach($fin as $f=>$r){
            $mtch = [];
            preg_match_all('/__'.$f.'[a-z_:]*/',$base,$mtch);
            foreach(array_shift($mtch) as $m){
                $len = strlen($m);
                $cpos = strpos($m,':');
                if($cpos !== FALSE)
                   $len = $cpos-2;

                if(substr($m,2,$len) != $f)
                    continue;

                if($cpos === FALSE){
                    $v = $r ==null||$r=='null'?'':$r;
                    if(strstr($m,'date') && $v != '')
                        $v = Date('D, d M Y ',strtotime($v));
                }
                else{
                    $v = $r ==null||$r=='null'?'':$r;
                    str_replace($v,' ','_');
                    $v = (strtolower($m) == strtolower('__'.$f.':'.$v))?'X':'';
                }

               // echo var_dump($m);
                //echo var_dump($v);
               $base = str_replace($m,$v,$base);
            }
        }


        $html = $pre.$base.$post;
        $pdfFilePath = $export->name.'.pdf';
        $pdf = $this->m_pdf->load();
        $pdf->img_dpi = 180;
        $pdf->WriteHTML($html);
        $pdf->Output($pdfFilePath, "D");

    }

    
    
}
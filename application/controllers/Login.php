<?php

/* 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 12/2015
 * Licensed to: LicensedTo
 *   License subject to changes based on agreement between  Author and Licensee * 
 */


if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {
    
        protected static $data = array('role'=>'','angular_controller'=>'loginCtrl','view'=>'login_view','ctrl_path'=>'login','model'=>'login_model');

  //Use the  construct to set default variables of the controller

      Public function __construct(){
           
          parent::__construct();
            } 
        


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function index()
	{   //$this->ion_auth->logout();
            if($this->ion_auth->logged_in())redirect($this->get_location ());
               
             $data = self::$data;
             $data['data']='';
             
             $this->load->view('base_view',$data);
            
	}
        
        
        public function Lost(){
                $data = self::$data;
                $data['data']='';
                $data['view'] = 'lost_view';
             
                $this->load->view('base_view',$data);
        }
	
	
	public function Logon(){
            
        $model = self::$data['model'];
        
        $uname = $this->input->post('uname') | '';
        $pass = $this->input->post('pass') | '';
        $remember = $this->input->post('remember') | '';
        if($uname){
        if($this->account_exists($uname)){

        $account_authenticated = $this->ion_auth->login($uname,$pass,$remember);
        if($account_authenticated){
            $this->load->library('users');
            $this->load->library('companies');
            $id = $this->session->userdata($this->users->get_join());
            //echo var_dump($this->users->find_by_id());die();
            $user_data = (array)$this->users->find_by_key($id);
            $this->session->set_userdata($user_data);
            if(parent::$user_app != 'mobile')
            echo 'ok';
            else{

                $this->load->config('appconfig', true);
                $app_access = $this->config->item('app_access', 'appconfig');

                //Check if request from mobille and if user group exists on mobile
                if (self::$user_app == 'mobile'){
                    $user_groups = $this->ion_auth->get_users_groups()->result()[0];
                    if(!in_array($user_groups->name,$app_access)){
                        $this->ion_auth->logout();
                        die('noaccess');
                    }
                }

                $user = $this->get_user_from_session();
                $user['role'] = $this->ion_auth->get_users_groups()->result()[0]->id;
                $token = $this->get_logged_user_details()->remember_code;
                $company = $this->companies->get_company_by_user($id);
                die(json_encode(
                    ['status'=>'ok','user'=>$user,'company'=>$company,'remember'=>['remember_code'=>$token,'identity'=>$this->session->identity]]
                ));
            }
        }
        else echo'fail';
      }
      else echo 'notexist';
      }
      else echo 'empty';

        die();
   	}

	
	
   public function change_password(){
        $identity = $this->session->identity;
        $old_pass = $this->input->post('old_pass');
        $new_pass = $this->input->post('new_pass');

       $pass_change = $this->ion_auth->change_password($identity,$old_pass,$new_pass);

       if($pass_change)
           die('ok');
       else die('no');
   }

   public function logout(){
    if($this->ion_auth->logout())echo 'ok';
       die();
   }
	
}



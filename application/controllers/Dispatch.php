<?php
/**
 * Copyright (c) 
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony 
 * Email: Adibas03@gmail.com
 * Date: 10/2016
 * Licensed to: LicensedTo
 * License subject to changes based on agreement between  Author and Licensee
 */


class Dispatch extends MY_Controller
{


    protected static $data = array('role' => 'Dispatch', 'angular_controller' => '', 'view' => '', 'ctrl_path' => 'dispatch', 'model' => '');

    function __construct()
    {
        parent::__construct();
        $user_type = isset($_REQUEST['user-type'])?$_REQUEST['user-type']:'';
        if(!$this->ion_auth->in_group('dispatchs'))redirect('login?user-type='.$user_type);

    }
    
    /*--Api Functions*/
    
    public function assigned(){
        $user = $this->ion_auth->user()->row()->id;
        
        $this->load->library('requests');
        $pendings = $this->requests->fetch_dispatch_pending_jobs($user);
        $this->send_json_response($pendings);
    }

    public function report(){
        $user = $this->ion_auth->user()->row()->id;

        $this->load->library('requests');
        $this->load->library('reports');

        $report = $this->input->post();

        $newreport = new $this->reports();
        $newrequest = new $this->requests();

        $newrequest->id = $newreport->request_id = $report['request_id'];

        if($newrequest->is_reported($newrequest->id))
            die('ok');

        $exempt = ['date_approved','date_updated'];

        foreach($newreport as $k=>$v){
            foreach($report as $n=>$r){
                if($newreport->$k == null && !in_array($k,$exempt))$newreport->$k = '';
                if($k==$n && $r!='') {
                    if($k=='color')
                        $newreport->$k = join(',',$r);
                    else
                        $newreport->$k = $r;
                    }
                }
            }

        $newreport->action = $newreport->action == ''? '':$newreport->action;
        $newreport->dispatch_id = $user;
        $newreport->admin_id = 0;
        //$newreport->date_creat = '0000-00-00 00:00:00';
        $newreport->date_created = date('Y-m-d H:i:s');

        $newrequest->status = $this->requests->get_state('reported');
        $newrequest->report_status = $this->reports->get_report_type($newreport->address_exists,$newreport->resides);

        $db = $this->db;
        $db->trans_start();

        $newreport->date_created = date('Y-m-d H:i:s');
        if($n_id = $newreport->create()){
            $newrequest->report_id = $n_id;
            $newrequest->status = $newrequest->get_state('reported');

            if(!$newrequest->update())
                die('newreportnotsavedtorequest');
        else {
                $db->trans_complete();
                die('ok');
            }
        }
        else
            die('reportnotcreated');

        }


}
<?php
/**
 * Copyright (c)
 * Address Verification (AVR) Portal
 * Author: Adegbemi Anthony
 * Email: Adibas03@gmail.com
 * Date: 10/2016
 * Licensed to: LicensedTo
 * License subject to changes based on agreement between  Author and Licensee
 */



class Client extends MY_Controller{


    protected static $data = array('role'=>'Client','angular_controller'=>'ClientCtrl','view'=>'index','ctrl_path'=>'client','model'=>'Client_model');



    function __construct(){
        parent::__construct();

        $user_type = isset($_REQUEST['user-type'])?$_REQUEST['user-type']:'';
        if(!$this->ion_auth->in_group('clients'))redirect('login?user-type='.$user_type);

        $this->load->model(self::$data['model']);
        $this->load->library('branches');
        $this->load->library('companies');

        $user_id = $this->session->userdata('user_id');
        //Reset branch to quelch incomplete class error
        $branch =$this->session->userdata('branch');
        $this->session->set_userdata('branch',$this->branches->get_instance((object)((array)$branch)));
        $branch =$this->session->userdata('branch');

        if(!$branch->id|| !$this->branches->get_company_branch_by_user($user_id,$this->session->userdata('branch')->id)){
            $model = self::$data['model'];
            $branch = $this->branches->get_branch_by_user($user_id);
            $this->session->set_userdata('branch',$branch);
        }
        $branch = $this->session->userdata('branch');

        self::$data['logo'] = $this->companies->find_by_id($branch->company_id)->logo;
        self::$data['branch'] = $branch;

        $this->load->config('appconfig',true);
        //load values from session
        self::$data['user'] = $this->get_user_from_session();

        //Initiate menu config
        $this->load->config('menus', true);
        self::$data['menu'] = $this->config->item('appmenus','menus')[self::$data['ctrl_path']];
    }


    /*--Pages Functions--*/

    public function index(){
        $data = self::$data;

        $data['menu'] = $this->config->item('appmenus','menus')[$data['ctrl_path']];

        $data['data'] = array();
        $this->load->view('base_view',$data);
        
    }

    public function pending(){
        $data = self::$data;
        $data['view'] = 'pending';


        $data['data'] = array();
        $this->load->view('base_view',$data);

    }

    public function submitted(){
        $data = self::$data;
        $data['view'] = 'submitted';

        $data['data'] = array();
        $this->load->view('base_view',$data);
    }



    public function preview($id,$rid){
        $data = self::$data;
        $data['view'] = 'preview';

        $responses = $this->config->item('report_responses','appconfig');

        $data['data'] = array('id'=>$id,'rid'=>$rid,'responses'=>$responses);
        $this->load->view('base_view',$data);

    }


    public function upload(){
        $data = self::$data;
        $data['view'] = 'upload';
        $data['upload_fields'] = $this->config->item('upload_fields','appconfig');
        $data['data'] = [];

        $this->load->view('base_view',$data);

    }








    /*--Additional Functions --*/

    public function status_count(){
        $this->load->library('requests');

        $data=[];
        $data['Pending'] = $this->requests->count_pending_by_branch($this->session->userdata('branch')->id);
        $data['Submitted'] = $this->requests->count_approved_by_branch($this->session->userdata('branch')->id);

        die(json_encode($data));
    }



    public function dashboard(){
        $this->load->library('requests');
        $this->load->library('reports');
        $this->load->library('companies');
        $this->load->library('branches');


        $cal = CAL_GREGORIAN;
        $lastsixmonths_data = [];
        $ss=['requests','reports'];

        $lastsixmonths_label = [date('M',strtotime('-6 months')),date('M',strtotime('-5 months')),date('M',strtotime('-4 months')),date('M',strtotime('-3 months')),date('M',strtotime('-2 months')),date('M',strtotime('-1 months'))];


        for($s=0;$s<2;$s++) {

            for ($m = 1; $m < 7; $m++) {
                if($s==0)
                    $lastsixmonths_data[$s][] =
                        $this->requests->fetch_by_var('count(id) as id', [
                            'client_branch_id'=>self::$data['branch']->id,
                            'request_date' => [date('Y-m-01 00:00:00', strtotime('-' . $m . ' months')), date('Y-m-' . cal_days_in_month($cal, date('n', strtotime('-' . $m . ' months')), date('Y', strtotime('-' . $m . ' months'))) . ' 23:59:59', strtotime('-' . $m . ' months'))]
                        ])[0]->id;
                else
                    $lastsixmonths_data[$s][] =
                        $this->requests->fetch_by_var('count(id) as id', [
                            'client_branch_id'=>self::$data['branch']->id,
                            'status' => 4,'request_date' => [date('Y-m-01 00:00:00', strtotime('-' . $m . ' months')), date('Y-m-' . cal_days_in_month($cal, date('n', strtotime('-' . $m . ' months')), date('Y', strtotime('-' . $m . ' months'))) . ' 23:59:59', strtotime('-' . $m . ' months'))]
                        ])[0]->id;
            }
        };


        for($s=0;$s<2;$s++) {

            for ($m = 1; $m < 7; $m++) {
                if ($s == 0)
                    $lastsixmonthsreports_data[$s][] =
                        $this->reports->fetch_by_var('count(id) as id', [
                            'address_exists' => 'yes',
                            'resides' => 'yes',
                            'id in (select report_id from requests where client_branch_id='.self::$data['branch']->id.' and status = 4 and request_date between "' . date('Y-m-01 00:00:00', strtotime('-' . $m . ' months')) . '" and "' . date('Y-m-' . cal_days_in_month($cal, date('n', strtotime('-' . $m . ' months')), date('Y', strtotime('-' . $m . ' months'))) . ' 23:59:59', strtotime('-' . $m . ' months')) . '")'
                        ])[0]->id;
                else
                    $lastsixmonthsreports_data[$s][] =
                        $this->reports->fetch_by_var('count(id) as id', [
                            '(address_exists!= "yes" or resides != "yes")',
                            'id in (select report_id from requests where client_branch_id='.self::$data['branch']->id.' and status = 4 and request_date between "' . date('Y-m-01 00:00:00', strtotime('-' . $m . ' months')) . '" and "' . date('Y-m-' . cal_days_in_month($cal, date('n', strtotime('-' . $m . ' months')), date('Y', strtotime('-' . $m . ' months'))) . ' 23:59:59', strtotime('-' . $m . ' months')) . '")'
                        ])[0]->id;
            }
        };


        $thismonths_label = [];

        for($d=1;$d<date('j')+1;$d++){
            $thismonths_label[] = $d;
        }

        $thismonthsrequest_data =[];

        for($s=0;$s<2;$s++) {

            for ($d = 1; $d < date('j') + 1; $d++) {
                $dd = str_pad ( $d , 2,'00',STR_PAD_LEFT );

                if ($s == 0)
                    $thismonthsrequest_data[$s][] =  $this->requests->fetch_by_var('count(id) as id', [
                        'client_branch_id'=>self::$data['branch']->id,
                        'request_date'=>[ date('Y-m-'.$dd.' 00:00:00', strtotime('now')), date('Y-m-'.$dd.' 23:59:59' , strtotime('now'))],
                    ])[0]->id;
                else
                    $thismonthsrequest_data[$s][] =  $this->requests->fetch_by_var('count(id) as id', [
                        'client_branch_id'=>self::$data['branch']->id,
                        'status'=>4,
                        'request_date'=>[ date('Y-m-'.$dd.' 00:00:00', strtotime('now')), date('Y-m-'.$dd.' 23:59:59' , strtotime('now'))],
                    ])[0]->id;
            }
        }


        $thismonthsreport_data =[];

        for($s=0;$s<2;$s++) {

            for ($d = 1; $d < date('j') + 1; $d++) {
                $dd = str_pad ( $d , 2,'00',STR_PAD_LEFT );

                if ($s == 0)
                    $thismonthsreport_data[$s][] =  $this->reports->fetch_by_var('count(id) as id', [
                        'address_exists' => 'yes',
                        'resides' => 'yes',
                        'date_created' => [ date('Y-m-'.$dd.' 00:00:00', strtotime('now')), date('Y-m-'.$dd.' 23:59:59' , strtotime('now'))],
                        'id in (select report_id from requests where client_branch_id='.self::$data['branch']->id.' and status = 4 )'
                    ])[0]->id;
                else
                    $thismonthsreport_data[$s][] =  $this->reports->fetch_by_var('count(id) as id', [
                        '(address_exists!= "yes" or resides != "yes")',
                        'date_created' => [ date('Y-m-'.$dd.' 00:00:00', strtotime('now')), date('Y-m-'.$dd.' 23:59:59' , strtotime('now'))],
                        'id in (select report_id from requests where client_branch_id='.self::$data['branch']->id.' and status = 4 )'
                    ])[0]->id;
            }
        }


        $thisdays_label = [];

        for($h=0;$h<date('G')+1;$h++){
            $thisdays_label[] = str_pad ( $h , 2,'00',STR_PAD_LEFT ).':00';
        }

        $thisdaysreport_data =[];

        for($s=0;$s<2;$s++) {

            for ($h = 0; $h < date('G') + 1; $h++) {
                $hh = str_pad ( $h , 2,'00',STR_PAD_LEFT );

                if ($s == 0)
                    $thisdaysreport_data[$s][] =  $this->reports->fetch_by_var('count(id) as id', [
                        'address_exists' => 'yes',
                        'resides' => 'yes',
                        'date_created'=>[date('Y-m-d '.$hh.':00:00', strtotime('now')),date('Y-m-d '.$hh.':59:59', strtotime('now'))],
                        'id in (select report_id from requests where client_branch_id='.self::$data['branch']->id. ' and status = 4 )'
                    ])[0]->id;
                else
                    $thisdaysreport_data[$s][] =  $this->reports->fetch_by_var('count(id) as id', [
                        '(address_exists!= "yes" or resides != "yes")',
                        'date_created'=>[date('Y-m-d '.$hh.':00:00', strtotime('now')),date('Y-m-d '.$hh.':59:59', strtotime('now'))],
                        'id in (select report_id from requests where client_branch_id='.self::$data['branch']->id.' and status = 4 )'
                    ])[0]->id;
            }
        }


        $thisdaysreports_label = ['yes','no','moved','locked'];

        $thisdayssummaryreport_data =[];

        for($s=0;$s<4;$s++) {
            $thisdayssummaryreport_data[] =  $this->reports->fetch_by_var('count(id) as id', [
                'resides' => $thisdaysreports_label[$s],
                'date_created'=>[date('Y-m-d 00:00:00', strtotime('now')),date('Y-m-d 23:59:59', strtotime('now'))],
                'id in (select report_id from requests where client_branch_id='.self::$data['branch']->id. ' and status = 4 )'
            ])[0]->id;
        }


        $dashboard['charts']=[
            [
                'title'=>"Today's report overview",
                'chartid'=>'pie',
                'chartclass'=>'chart-pie',
                'labels'=>$thisdaysreports_label,
                'data'=>[$thisdayssummaryreport_data]
            ],
            [
                'title'=>"Today's report density",
                'chartid'=>'line',
                'chartclass'=>'chart-bar',
                'series'=>['Positive','Negative'],
                'labels'=>$thisdays_label,
                'data'=>$thisdaysreport_data
            ],
            [
                'title'=>"This month's requests",
                'chartid'=>'bar',
                'chartclass'=>'chart-bar',
                'series'=>['Requests','Submitted'],
                'labels'=>$thismonths_label,
                'data'=>$thismonthsrequest_data
            ],
            [
                'title'=>"This month's reports",
                'chartid'=>'line',
                'chartclass'=>'chart-line',
                'series'=>['Positive','Negative'],
                'labels'=>$thismonths_label,
                'data'=>$thismonthsreport_data
            ],
            [
                'title'=>"Previous Six months requests",
                'chartid'=>'bar',
                'chartclass'=>'chart-bar',
                'series'=>['Requests','Submitted'],
                'labels'=>$lastsixmonths_label,
                'data'=>$lastsixmonths_data
            ],
            [
                'title'=>"Previous Six months reports",
                'chartid'=>'line',
                'chartclass'=>'chart-line',
                'series'=>['Positive','Negative'],
                'labels'=>$lastsixmonths_label,
                'data'=>$lastsixmonthsreports_data
            ]
        ];

        die(json_encode($dashboard));

    }



    public function pending_requests(){
        $this->load->library('requests');
        $req = $this->requests->find_pending_by_branch($this->session->userdata('branch')->id);
        $reqs = array();
        if($req)
            foreach($req as $one){
                $one->dispatch_id = $this->requests->fetch_request_dispatch($one->id);
                $reqs[] = $one;
            }
        echo json_encode($reqs);
    }


    public function approved_requests(){
        $this->load->library('requests');
        echo json_encode($this->requests->find_approved_by_branch($this->session->userdata('branch')->id));
    }


    public function submitted_requests_preview(){
        $this->load->library('requests');
        $this->load->library('reports');
        $branch = $this->session->userdata('branch')->id;
        $requests = $this->requests->fetch_group_members_info('approved',array('id','name','address','status','report_id','report_status'),array('client_branch_id'=>$branch));
        $reports = array();
        ///print_r($requests);
        if($requests){
            foreach($requests as $req){
                $rep = $this->reports->fetch_report_preview($req->report_id);
                //print_r($rep);
                if(is_array($rep))$req->report = array_shift($rep);
                $reports[] = $req;
                //print_r($req);
            }
            echo json_encode($reports);
        }
        else return false;
    }


    public function fetch_request($id){
        $this->load->library('requests');
        $req = $this->requests->find_by_id($id);
        //if($req)$req->dispatch_id = $this->requests->fetch_request_dispatch($id);
        $req->dispatch =  $this->requests->fetch_request_dispatch_info($id);
        echo json_encode($req);
    }


    public function fetch_report($id){
        $this->load->library('reports');
        $rep = $this->reports-> find_by_id($id);

        echo json_encode($rep);
    }

    public function new_uploads(){
        $uploads = $this->input->post('requests');
        $errors = [];

        $this->load->library('requests');
        if(count($uploads)<1)die('empty');

        $batch = $this->requests->generate_batch();
        foreach($uploads as $upload){
            $new_request = new $this->requests;

            foreach($new_request as $k=>$v){
                if(isset($upload[$k]) &&$upload[$k] != '')$new_request->$k = $upload[$k];
            }
            $new_request->batch_id = $batch;
            $new_request->client_branch_id = $this->session->userdata('branch')->id;

            //Set/Format Request date
            if(strpos($new_request->request_date,'/')>-1)$new_request->request_date = str_replace('/', '-', $new_request->request_date);
            $new_request->request_date = $new_request->request_date == ''?date('Y-m-d H:i:s'):date('Y-m-d H:i:s',strtotime($new_request->request_date));

            if(!$new_request->create())
                $errors[] = $new_request->name;
        }

        if(count($errors) < 1)die('ok');
        else die(json_encode($errors));
    }

    public function client_branches(){
        $model = self::$data['model'];
        $this->load->library('branches');
        $branches = $this->branches->fetch_all_company_branches(Companies::get_company_by_user($this->session->userdata('user_id'))->id);
        die(json_encode($branches));
    }


    public function change_active_branch(){
        $this->load->library('branches');
        $new_client = $this->input->post('branch');
        $user_id = $this->session->userdata('user_id');
        $model = self::$data['model'];

        $new_branch = $this->branches->get_company_branch_by_user($user_id,$new_client);

        if($new_branch){
            $this->session->set_userdata('branch',$new_branch);
            die('ok');
        }
        else die('no');

    }



}
